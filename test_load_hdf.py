#!/usr/bin/env python

import argparse
import h5py
import time
import sys
import numpy as np

from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import chi2
from sklearn.svm import LinearSVC
from sklearn.model_selection import cross_val_score

parser = argparse.ArgumentParser(description='script for loading of HDF5 file')
parser.add_argument('-x',help='dataset\'s X HDF5 file')
parser.add_argument('-f',help='dataset\'s feature names csv file')
parser.add_argument('-y',help='dataset\'s Y csv file')
args = parser.parse_args()

start_time=time.time()
f = [line.rstrip('\n') for line in open(args.f)]

h5f = h5py.File(args.x,'r')
X = h5f['dataset'][:]
print 'shape of X: {}'.format(X.shape)
h5f.close()
print 'time to load HDF5 data: {}'.format(time.time()-start_time)
sys.stdout.flush()

y = np.loadtxt(args.y,dtype=np.dtype(np.str_),delimiter=',')

print 'time to load feature data: {}'.format(time.time()-start_time)
sys.stdout.flush()

start_time=time.time()
print 'shape before fs: {}'.format(X.shape)

print 'Performing feature selection..'
sys.stdout.flush()
sel = SelectKBest(chi2, k=2000)
#sel = VarianceThreshold(threshold=(.8 * (1 - .8)))
X_new = sel.fit_transform(X,y)

print 'shape after fs: {}'.format(X_new.shape)
sup = sel.get_support()
print 'support: {}'.format(sup)

selected_feature_names = [n for (i,n) in enumerate(f) if sup[i]]
print 'selected feature names:'
print selected_feature_names

print 'time to perform feature selection: {}'.format(time.time()-start_time)

print 'Creating pipeline with feature selection and running cross-validation...'
sys.stdout.flush()
start_time = time.time()
#clf = make_pipeline(VarianceThreshold(threshold=(.8 * (1 - .8))),LinearSVC(C=0.01,penalty="l1",dual=False))
clf = LinearSVC(C=0.01,penalty="l1",dual=False)
print 'X.shape: {}'.format(X.shape)
print 'y.shape: {}'.format(y.shape)
print 'cross-validation result:'
scores = cross_val_score(clf,X_new,y,cv=10)
print 'time to run pipeline with cross-validation: {}'.format(time.time()-start_time)

print scores
print("Accuracy: %0.2f (+/- %0.2f)" % (scores.mean(), scores.std() * 2))
