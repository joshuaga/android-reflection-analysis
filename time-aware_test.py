#!/usr/bin/env python

import h5py
import numpy as np
import argparse
import csv
import utils
import time
import sys

from datetime import date

prog_start_time =time.time()

parser = argparse.ArgumentParser(description='performs training and testing on APK dataset based on date mappings from a CSV file')
parser.add_argument('--hdf',help='HDF5 file with APK dataset',required=True)
parser.add_argument('--dates-file',help='CSV file with date mapping',required=True)
parser.add_argument('--split-date',help='date in YYYY-MM-DD format',required=False)
args = parser.parse_args()
print args

DEBUG=False
apk_dates = dict()
with open(args.dates_file,'rb') as csvfile:
    datereader = csv.reader(csvfile,delimiter=',')
    next(datereader,None) # skip header row
    for row in datereader:
        if len(row) >= 6:
            apk_name = row[0].replace('VirusShare_','').replace('.apk','')
            dex_year = row[3]
            dex_month = row[4]
            dex_day = row[5]
            try:
                dex_date = date(int(dex_year),int(dex_month),int(dex_day))
                apk_dates[apk_name]=dex_date
            except ValueError as err:
                if DEBUG:
                    print 'The following year - {}, month -{}, day - {}, threw the following error: {}'.format(dex_year,dex_month,dex_day,err)
        else:
            continue

if DEBUG:
    for apk_name,apk_date in apk_dates.items():
        print apk_name, apk_date
print 'Number of valid dates extracted: {}'.format(len(apk_dates))
sys.stdout.flush()

split_date=None
if args.split_date:
    tokens = args.split_date.split('-')
    split_year = int(tokens[0])
    split_month = int(tokens[1])
    split_day = int(tokens[2])
    split_date = date(split_year,split_month,split_day)
else:
    first_of_2014=date(2014,1,1)
    split_date=first_of_2014

h5f = h5py.File(args.hdf,'r')
apk_names = h5f['apk_names'][:]
y = h5f['y'][:]

apks_train = [] 
X_train = []
y_train = []

apks_test = []
X_test = []
y_test = []

check_size = 1000
stop_size=sys.maxint
start_time = time.time()
for i,apk_name in enumerate(apk_names):
    if len(X_train) % check_size == 0:
        print 'X_train length: {}'.format(len(X_train))
        print 'Training time so far: {}'.format(time.time()-start_time)
    if len(X_test) % check_size == 0:
        print 'X_test length: {}'.format(len(X_test))
        print 'Training time so far: {}'.format(time.time()-start_time)
    if len(X_train) >= stop_size and len(X_test) >= stop_size:
        break
    if apk_name in apk_dates.keys():
        if apk_dates[apk_name] < split_date:
            apks_train.append(apk_name)
            X_train.append(h5f['X'][i])
            y_train.append(y[i])
        else:
            apks_test.append(apk_name)
            X_test.append(h5f['X'][i])
            y_test.append(y[i])
print 'training set size: {}'.format(len(X_train))
print 'testing set size: {}'.format(len(X_test))
print 'time to create training and testing sets: {}'.format(time.time()-start_time)
sys.stdout.flush()

start_time=time.time()
clf = utils.select_classifier('d')
clf = clf.fit(X_train,y_train)
print 'training time: {}'.format(time.time()-start_time)
sys.stdout.flush()

start_time=time.time()
predictions = clf.predict(X_test)
print 'prediction time: {}'.format(time.time()-start_time)
sys.stdout.flush()

utils.print_classification_report(y_test,predictions)
utils.print_confusion_matrix(y_test,predictions)

print 'total execution time: {}'.format(time.time()-prog_start_time)
sys.stdout.flush()
