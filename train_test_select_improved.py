#!/usr/bin/env python

import time
import argparse
import sys
import utils
import numpy as np

from os.path import join
from scipy.sparse import lil_matrix, vstack

def main():
    prog_start_time = time.time()

    parser = argparse.ArgumentParser(description='train and test using selected list of apps')
    parser.add_argument('--train-list',help='input app list for training',required=False)
    parser.add_argument('--test-list',help='input app list for testing',required=False)
    parser.add_argument('-n',help='input csv file with names of dataset')
    parser.add_argument('-x',help='input csv file with X dataset')
    parser.add_argument('-y',help='input csv file with y dataset')
    parser.add_argument('--hdf',help='input HDF file with dataset')
    parser.add_argument('-c',metavar='C',default='d',help='select a classifier: k for nearest neighbor, d for decision tree (the default), s for SVM, l for linear SVC')
    parser.add_argument('--train-arff',help='arff file to use for filtering training set')
    parser.add_argument('--test-arff',help='arff file to use for filtering testing set')
    parser.add_argument('--no-family',action='store_true',required=False,help='replace family labels with malware label')
    args = parser.parse_args()

    print args
    sys.stdout.flush()

    if not args.train_list:
        print 'no training list provided'
    if not args.test_list:
        print 'no testing list provided'

    only_one_arff = False
    if args.train_arff and not args.test_arff:
        only_one_arff = True
    if args.test_arff and not args.train_arff:
        only_one_arff = True

    if only_one_arff:
        raise StandardError('You cannot supply only --train-arff or only --test-arff, you need both or neither')
        
    print 'loading filtering arff files'
    sys.stdout.flush()
    if args.train_arff and args.test_arff:
        names_train_filter,X_train_filter,y_train_filter,dataset_train_filter = utils.extract_arff_data(args.train_arff)
        names_test_filter,X_test_filter,y_test_filter,dataset_test_filter = utils.extract_arff_data(args.test_arff)

    print 'loading selected app lists'
    sys.stdout.flush()

    if args.train_list and args.test_list:
        train_list = [line.rstrip('\n').replace('VirusShare_','') for line in open(args.train_list)]
        test_list = [line.rstrip('\n').replace('VirusShare_','') for line in open(args.test_list)]
        if args.train_arff and args.test_arff:
            train_set = set(train_list).intersection(names_train_filter)
            test_set = set(test_list).intersection(names_test_filter)
        else:
            train_set = set(train_list)
            test_set = set(test_list)
    elif args.train_arff and args.test_arff:
        train_set = set(names_train_filter)
        test_set = set(names_test_filter)
    else:
        raise StandardError('Based on your arguments, I don\'t know what you want to do')

    DEBUG=False
    if DEBUG:
        print 'training list'
        print train_list
        print 'testing list'
        print test_list

    print 'size of training set from training file: {}'.format(len(train_set))
    print 'size of testing set from testing file: {}'.format(len(test_set))
    sys.stdout.flush()

    start_time=time.time()
    if args.x and args.y and args.n:
        y, X, names = load_csv_files(args)
    elif args.hdf:
        names,X,y,fnames = utils.extract_hdf_data(args.hdf)
    else:
        raise StandardError('You can provide CSV files or HDF files. You\'ve supplied an invalid dataset file combination as input.')
    print 'time to load data from filesystem: {}'.format(time.time()-start_time)
    sys.stdout.flush()
    
    print 'converting X to lil_matrix'
    sys.stdout.flush()
    start_time=time.time()
    X=lil_matrix(X)
    print 'time to convert X to lil_matrix: {}'.format(time.time()-start_time)
    sys.stdout.flush()
    
    names_fixed = []
    for name in names:
        name = name.replace('VirusShare_','',1).replace('malware_','',1)
        if not name.endswith('.apk'):
            names_fixed.append(name + '.apk')
        else:
            names_fixed.append(name)
    names=names_fixed

    train_size=0
    for app in train_set:
        for i,name in enumerate(names):
            if name == app:
                train_size=train_size+1

    names_train=[]
    X_train=lil_matrix( (train_size,len(fnames)),dtype=np.int )
    y_train=[]

    test_size=0
    for app in test_set:
        for i,name in enumerate(names):
            if name == app:
                test_size=test_size+1

    names_test=[]
    X_test=lil_matrix( (test_size,len(fnames)),dtype=np.int )
    y_test=[]

    names_set = set(names)

    print 'building missing training and testing apps...'
    sys.stdout.flush()
    start_time=time.time()
    train_missing = train_set.difference(names_set)
    test_missing = test_set.difference(names_set)

    rmf = open(join('res','ttsel_train_missing.csv'),'w')
    emf = open(join('res','ttsel_test_missing.csv'),'w')

    for name in train_missing:
        rmf.write('{}\n'.format(name))
    rmf.close()
        
    for name in test_missing:
        emf.write('{}\n'.format(name))
    emf.close()
    
    print 'time to build missing training and testing apps list: {}'.format(time.time()-start_time)
    sys.stdout.flush()

    print 'building training set...'
    sys.stdout.flush()
    start_time = time.time()
    train_row=0
    for app in train_set:
        for i,name in enumerate(names):
            if name == app:
                names_train.append(names[i])
                #X_train.append(X[i])
                #X_train = vstack(X_train,X[i])
                for j in range(X[i].shape[1]):
                    #if X[i,j] > 0:
                    #    X_train[train_row,j]=X[i,j]
                    pass
                if y[i] != 'Malware' and y[i] != 'Benign' and args.no_family:
                    #if DEBUG:
                    #    print 'Changing {} to Malware'.format(y[i])
                    #y_train.append('Malware')
                    pass
                else:
                    y_train.append(y[i])
                    pass
                train_row=train_row+1
    print 'time to build training set: {}'.format(time.time()-start_time)
    sys.stdout.flush()

    return

    print 'X_train:\n{}'.format(X_train.toarray())
    #X_train.toarray()

    print 'building testing set...'
    sys.stdout.flush()
    start_time = time.time()
    test_row=0
    for app in test_set:
        for i,name in enumerate(names):
            if name == app:
                names_test.append(names[i])
                #X_test.append(X[i])
                #X_test(X_test,X[i])
                for j in range(X[i].shape[1]):
                    if X[i,j] > 0:
                        X_test[test_row,j]=X[i,j]
                if y[i] != 'Malware' and y[i] != 'Benign' and args.no_family:
                    if DEBUG:
                        print 'Changing {} to Malware'.format(y[i])
                    y_test.append('Malware')
                else:
                    y_test.append(y[i])
                test_row=test_row+1
    print 'time to build testing set: {}'.format(time.time()-start_time)
    sys.stdout.flush()

    print 'X_test:\n{}'.format(X_test.toarray())
    #X_test.toarray()
    
    print 'training dataset size: {}'.format(len(names_train))
    print 'testing dataset size: {}'.format(len(names_test))

    #X_train = np.array(X_train)
    y_train = np.array(y_train)
    #X_test = np.array(X_test)
    y_test = np.array(y_test)

    print 'y_train: {}'.format(y_train)
    print 'y_test: {}'.format(y_test)

    print 'running training and testing'
    prog_time=time.time()
    clf = utils.select_classifier_and_fit(args.c,X_train,y_train)
    predicted = clf.predict(X_test)
    print 'time to train and test classifier: {}'.format(time.time()-prog_time)
    
    print utils.print_classification_report(y_test,predicted)
                                             
    print 'total execution time: {}'.format(time.time() - prog_start_time)

def load_csv_files(args):
    print 'loading csv files'
    sys.stdout.flush()
    sec_time=time.time()
    names = np.loadtxt(args.n,dtype=np.dtype(np.str_),delimiter=',')
    X = np.loadtxt(args.x,delimiter=',')
    y = np.loadtxt(args.y,dtype=np.dtype(np.str_),delimiter=',')
    print 'time to load csv files: {}'.format(time.time()-sec_time)
    return y, X, names

if __name__ == '__main__':
    main()
