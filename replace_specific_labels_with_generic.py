#!/usr/bin/env python

import h5py
import argparse

parser = argparse.ArgumentParser(description='replace any labels that are now Benign or unknown with Malware in an HDF5 file')
parser.add_argument('--hdf',help='input HDF5 file to be modified')
args = parser.parse_args()

f = h5py.File(args.hdf)
y = f['y']

for i,label in enumerate(y):
    if label != 'Benign' and label != 'unknown':
        y[i] = 'Malware'

f.close()
