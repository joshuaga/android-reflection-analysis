#!/usr/bin/env python

import time
import argparse
import sys
import numpy as np
import utils
import re
import pandas as pd

def main():
    prog_start_time = time.time()

    parser = argparse.ArgumentParser(description='split, train, and test from csv files')
    parser.add_argument('-n',help='input csv file with instance names')
    parser.add_argument('-x',help='input csv file with X dataset')
    parser.add_argument('-y',help='input csv file with y dataset')
    parser.add_argument('--train-size',default=1109,help='select this number of instances as training (from the end of the dataset) and the rest as testing')
    parser.add_argument('-c',metavar='C',default='d',help='select a classifier: k for nearest neighbor, d for decision tree (the default), s for SVM, l for linear SVC')
    parser.add_argument('--do-ref-test',action='store_true',help='train on every instance without name \'ref_*\', test on the remainder',required=False)
    parser.add_argument('--do-omg-test',action='store_true',help='test on obfuscated malgenome, train on others except original malgenome',required=False)
    parser.add_argument('--do-am-test',action='store_true',help='test on anzhi malware, train on others',required=False)
    args = parser.parse_args()

    print 'loading csv files'
    sys.stdout.flush()
    print 'loading instance names'
    sys.stdout.flush()
    apk_names = np.loadtxt(args.n,dtype=np.dtype(np.str_),delimiter=',')
    print 'loading X data'
    sys.stdout.flush()
    #X = pd.read_csv(args.x,delimiter=',').values
    X = np.loadtxt(args.x,delimiter=',')
    print 'loading y data'
    sys.stdout.flush()
    y = np.loadtxt(args.y,dtype=np.dtype(np.str_),delimiter=',')

    print 'splitting dataset for training and testing'
    sys.stdout.flush()
    start_time = time.time()

    test_flags = [args.do_ref_test,args.do_omg_test,args.do_am_test]
    flag_count=0
    for flag in test_flags:
        if flag:
            flag_count+=1
    if flag_count > 1:
        raise StandardError('Must choose only one of ref test, am test, or omg test')
    
    if args.do_ref_test:
        n_train = []
        X_train = []
        y_train = []

        n_test = []
        X_test = []
        y_test = []
        
        print 'selecting \'ref_*\' apks for reflection transformation test'
        sys.stdout.flush()
        for i,apk_name in enumerate(apk_names):
            if apk_name.startswith('ref_'):
                n_test.append(apk_name)
                X_test.append(X[i])
                if y[i] != 'Malware' or y[i] != 'Benign':
                    y_test.append('Malware')
                else:
                    y_test.append(y[i])
            else:                
                n_train.append(apk_name)
                X_train.append(X[i])
                y_train.append(y[i])

        print 'training size: {}'.format(len(n_train))
        print 'testing size: {}'.format(len(n_test))
    elif args.do_am_test:
        n_train = []
        X_train = []
        y_train = []

        n_test = []
        X_test = []
        y_test = []
        
        print 'selecting anzhi malware apks for testing'
        sys.stdout.flush()
        for i,apk_name in enumerate(apk_names):
            if apk_name.startswith('malware_'):
                n_test.append(apk_name)
                X_test.append(X[i])
                y_test.append(y[i])
            else:                
                n_train.append(apk_name)
                X_train.append(X[i])
                y_train.append(y[i])

        print 'training size: {}'.format(len(n_train))
        print 'testing size: {}'.format(len(n_test))
    elif args.do_omg_test:
        print 'selecting files for malgenome transformations without reflection'
        sys.stdout.flush()
        p = r'^t[0-9]_([A-za-z0-9]+)\.apk'
        malgenome_names=[]

        n_train=[]
        X_train=[]
        y_train=[]

        n_test = []
        X_test = []
        y_test = []
        
        for i,n in enumerate(apk_names):
            m = re.match(p,n)
            if m: # if the apk is transformed
                n_test.append(n)
                X_test.append(X[i])
                if y[i] != 'Malware' and y[i] != 'Benign':
                    y_test.append('Malware')
                else:
                    y_test.append(y[i])
                malgenome_names.append(m.group(1) + '.apk')

        for i,n in enumerate(apk_names):
            if not n in malgenome_names:
                n_train.append(n)
                X_train.append(X[i])
                if y[i] != 'Malware' and y[i] != 'Benign':
                    y_train.append('Malware')
                else:
                    y_train.append(y[i])

        print 'training size: {}'.format(len(n_train))
        print 'testing size: {}'.format(len(n_test))
    else:
        print 'selecting the last {} instances for training and the rest for testing'.format(args.train_size)
        sys.stdout.flush()
        X_train = X[args.train_size:]
        y_train = y[args.train_size:]
        X_test = X[:args.train_size]
        y_test = y[:args.train_size]
            
    print 'time to split dataset into training and testing: {}'.format(time.time()-start_time)
        
    
    print 'running training and testing'
    sys.stdout.flush()
    start_time = time.time()
    clf = utils.select_classifier_and_fit(args.c,X_train,y_train)
    predicted = clf.predict(X_test)
    print 'time to train and test classifier: {}'.format(time.time()-start_time)
    
    print utils.print_classification_report(y_test,predicted)
    print 'total execution time: {}'.format(time.time()-prog_start_time)



if __name__ == '__main__':
    main()
