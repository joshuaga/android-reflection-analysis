#!/usr/bin/env python

import argparse
from os import listdir
from os.path import isfile, join
import numpy as np

import time
import os
import logging
import sys
import utils
import re

from sklearn.feature_extraction import DictVectorizer
from sklearn import cross_validation
from sklearn import metrics
from sklearn import tree

from multiprocessing import Pool
import multiprocessing
import threading
import functools

dataset_lock = threading.Lock()
pool = Pool(multiprocessing.cpu_count())

def to_uni_array(array_unicode):
    items = [x.encode('utf-8') for x in array_unicode]
    array_unicode = np.array([items]) # remove the brackets for line breaks
    return array_unicode

def main():
    start_time = time.time()
    v = DictVectorizer(sparse=False)

    parser = argparse.ArgumentParser(description="convert a directory of log files to a feature vector")
    parser.add_argument('dir',help='directory with feature or log files')
    parser.add_argument('--arff',help='input arff file',required=False)
    parser.add_argument('--json',help='json file with dataset',required=False)
    parser.add_argument('--hdf',help='HDF5 file with dataset',required=False)
    parser.add_argument('--n-folds',type=int,default=10)
    args = parser.parse_args()

    timestr = time.strftime("%Y%m%d-%H%M%S")    

    if args.arff:
        loaded_apk_names,X,y,dataset = utils.extract_arff_data(args.arff)
        input_file=args.arff
    elif args.json:
        loaded_apk_names,X,y,feature_names=utils.extract_json_data(args.json)
        input_file=args.json
    elif args.hdf:
        loaded_apk_names,X,y,feature_names=utils.extract_hdf_data(args.hdf)
        input_file=args.hdf
    else:
        raise StandardError('You need to provide an arff or json file as input')

    loaded_apk_names=[re.sub('_fapi.txt$','',n) for n in loaded_apk_names]
    
    logger_file = 'logs/' + os.path.basename(__file__) + '-' + os.path.basename(input_file) + '_' + timestr + '.log'
    logging.basicConfig(filename=logger_file,level=logging.DEBUG,format='%(thread)d:%(threadName)s:%(levelname)s:%(name)s:%(funcName)s:%(message)s')
    print 'this script\'s name: ' + os.path.basename(__file__)
    print 'input dataset file: ' + os.path.basename(input_file)
    print 'will log to ' + logger_file
    sys.stdout.flush()

    #fmf = open('android-22-method_names.txt','r')
    #full_method_names = [m for m in fmf]
    
    print('execution time to load dataset file: %s' % (time.time() - start_time))
    sys.stdout.flush()

    print 'extracting reflect features from reflect files'
    sys.stdout.flush()
    erffl_start_time = time.time()
    reflect_apk_names, X_reflect, reflect_feature_names = extract_features_from_reflect_files(args.dir, v)
    print 'time to run extract_features_from_reflect_files: {}'.format(time.time()-erffl_start_time)
    sys.stdout.flush()

    reflect_apk_names=[re.sub('\.apk$','',n) for n in reflect_apk_names]

    print 'storing reflect features as csv files'
    sys.stdout.flush()
    reflect_store_start_time = time.time()
    RES="res/"
    np_x_reflect_only_csv = join(RES,'X_reflect_only-' + os.path.basename(input_file) + '_x_' + timestr + '.csv')
    np_names_reflect_only_csv = join(RES,'X_reflect_only-' + os.path.basename(input_file) + '_names_' + timestr + '.csv')
    np.savetxt(np_names_reflect_only_csv,np.array(reflect_apk_names),delimiter=',',fmt="%s")
    np.savetxt(np_x_reflect_only_csv,np.array(X_reflect).astype(np.float_),delimiter=',',fmt="%f")    
    print 'time to run storage of reflect features as csv files: {}'.format(time.time()-reflect_store_start_time)
    
    #X_combined = []
    print 'combining old and new features'
    sys.stdout.flush()
    combine_start_time = time.time()
    new_apk_names, X_combined, y_combined  = utils.combine_feature_sets(loaded_apk_names, reflect_apk_names, X, X_reflect, y)
    print 'time to combine old and new features: {}'.format(time.time()-combine_start_time)
    sys.stdout.flush()

    assert new_apk_names.shape[0] == X_combined.shape[0] and y_combined.shape[0] == X_combined.shape[0]
    for i in range(new_apk_names.shape[0]):
        logging.debug(new_apk_names[i])
        logging.debug(X_combined[i])
        logging.debug(y_combined[i])

    print 'adding zero features for instances without new features'
    sys.stdout.flush()
    zf_start_time = time.time()
    new_apk_names, y_combined, X_combined = utils.add_zero_features_to_missing(X_reflect, new_apk_names, loaded_apk_names, y_combined, X_combined, y, X)
    print 'time to add zero features: {}'.format(time.time()-zf_start_time)
    sys.stdout.flush()

    #npz_file = join("res",os.path.basename(__file__) + '-' + os.path.basename(args.arff) + '_' + timestr + '.npz')

    print 'saving dataset to csv files'
    sys.stdout.flush()
    save_start_time = time.time()

    if not os.path.exists(RES):
        os.makedirs(RES)

    np_names_csv = join(RES,os.path.basename(input_file) + '_reflect_names_' + timestr + '.csv')
    np_x_csv = join(RES,os.path.basename(input_file) + '_reflect_x_' + timestr + '.csv')
    np_y_csv = join(RES,os.path.basename(input_file) + '_reflect_y_' + timestr + '.csv')
    np_fnames_csv = join(RES,os.path.basename(input_file) + '_reflect_fnames_' + timestr + '.csv')

    #np.savez(npz_file,apk_names=new_apk_names,X=X_combined,y=y_combined)
    np.savetxt(np_names_csv,new_apk_names,delimiter=',',fmt="%s")
    np.savetxt(np_x_csv,X_combined,delimiter=',',fmt="%f")
    np.savetxt(np_y_csv,y_combined,delimiter=',',fmt="%s")

    combined_features=[]
    if args.arff:
        fnames = [i[0] for i in dataset['attributes']]
        fnames=fnames[1:-1]
        combined_features.extend(fnames)
        combined_features.extend(reflect_feature_names)
    elif args.json or args.hdf:
        np.savetxt(np_fnames_csv,to_uni_array(feature_names),delimiter='\n',fmt="%s")
        combined_features.extend(feature_names)
        combined_features.extend(reflect_feature_names)

    combined_features=np.array(combined_features)
    np.savetxt(np_fnames_csv,to_uni_array(combined_features),delimiter='\n',fmt="%s")
     
    print 'time to save new dataset: {}'.format(time.time()-save_start_time)
    sys.stdout.flush()

    print 'running classifier on new dataset'
    sys.stdout.flush()
    clf_start_time = time.time()
    clf = tree.DecisionTreeClassifier()

    #scores = cross_validation.cross_val_score(clf, X_combined, y_combined, cv=2)
    #print("Accuracy: %0.2f (+/- %02.f)" % (scores.mean(), scores.std()*2))

    print 'performing cross-validation using {} folds'.format(args.n_folds)
    predicted = cross_validation.cross_val_predict(clf, X_combined, y_combined, cv=args.n_folds)
    print 'accuracy: {}'.format(metrics.accuracy_score(y_combined,predicted))

    utils.print_classification_report(y_combined,predicted)
    print 'time to perform classification on new dataset: {}'.format(time.time()-clf_start_time)

    print('total execution time: %s' % (time.time() - start_time))

def extract_reflect_features_from_logs(logs_dir, v):
    onlyfiles = [f for f in listdir(logs_dir) if isfile(join(logs_dir,f)) and f.startswith('edu.uci.seal.cases.analyses.ReflectUsageTransformer-') and f.endswith('.log')]

    dataset=[]
    reflect_apk_names=[]
    reflect_class_counts = dict()

    MAX_FILES=60000
    file_limit = MAX_FILES if MAX_FILES <= len(onlyfiles) else len(onlyfiles)

    def create_reflect_feature_vector(f,logs_dir):
        REFLECT_INVOKE_COUNT='reflect_invoke_count'
        NS_CONST_METHOD_COUNT='nonstring_constant_method_count'
        METHOD_NO_CLASS_COUNT='method_no_class_count'
        FULL_METHOD_COUNT='full_method_count'
    
        fv = dict()
        parsing_full_method_counts = False
        parsing_part_method_counts = False
        apk_name = f.replace('edu.uci.seal.cases.analyses.ReflectUsageTransformer-','')
        apk_name = apk_name.replace('-main.log','')
        logging.debug(apk_name)
    
        fp = open(join(logs_dir,f),'r')
        part_method_counter=0
        for line in fp:
            if 'reflect_invoke_count:' in line:
                fv[REFLECT_INVOKE_COUNT] = int(line.strip().split(' ')[-1])
                logging.debug( REFLECT_INVOKE_COUNT + ': {}'.format(fv[REFLECT_INVOKE_COUNT]) )
            if 'nonstring_constant_method_count:' in line:
                fv[NS_CONST_METHOD_COUNT] = int(line.strip().split(' ')[-1])
                logging.debug( NS_CONST_METHOD_COUNT + ': {}'.format(fv[NS_CONST_METHOD_COUNT]) )
            if 'method_no_class_count: ' in line:
                fv[METHOD_NO_CLASS_COUNT] = int(line.strip().split(' ')[-1])
                logging.debug( METHOD_NO_CLASS_COUNT + ': {}'.format(fv[METHOD_NO_CLASS_COUNT]) )
            if 'full_method_count: ' in line:
                fv[FULL_METHOD_COUNT] = int(line.strip().split(' ')[-1])
                logging.debug( FULL_METHOD_COUNT + ': {}'.format(fv[FULL_METHOD_COUNT]) )
    
            if 'Partial method counts:' in line:
                parsing_full_method_counts = False
                parsing_part_method_counts = True
    
            if parsing_full_method_counts and not parsing_part_method_counts:
                tokens = line.split('|')[-1].split(':')
                full_method_name = tokens[-2].strip()
                count = tokens[-1].strip()
                fv[full_method_name] = int(count)
                logging.debug( full_method_name + ': {}'.format(fv[full_method_name]) )
    
            if parsing_part_method_counts:
                if not 'total runtime' in line and not 'Reached end' in line and not 'Partial method counts' in line and not line.isspace():
                    part_method_counter=part_method_counter+1
                    tokens = line.split('|')[-1].split(':')
                    part_method_name = tokens[-2].strip()
                    part_count = tokens[-1].strip()
                    fv[part_method_name]=int(part_count)
                    logging.debug( part_method_name + ': {}'.format(fv[part_method_name]) )
            if 'Full method counts:' in line:
                parsing_full_method_counts = True
        fv['part_method_counter']=part_method_counter
        logging.debug( 'part_method_counter: {}'.format(fv['part_method_counter']) )
        logging.debug( fv )
        #dataset.append(fv)
        logging.debug('')
        return apk_name,fv
    
    results = utils.parmap(functools.partial(create_reflect_feature_vector,logs_dir=logs_dir),onlyfiles[:file_limit])

    reflect_apk_names = [r[0] for r in results]
    dataset = [r[1] for r in results]

    X_reflect = v.fit_transform(dataset)
    for i,row in enumerate(X_reflect):
        logging.debug( '{},{}'.format(reflect_apk_names[i], row) )

    logging.debug("reflect feature names:")
    logging.debug(v.get_feature_names())
    
    logging.debug('reflect class counts:')
    logging.debug(reflect_class_counts)
    return reflect_apk_names, X_reflect, v.get_feature_names()

def extract_features_from_reflect_files(reflect_dir, v):
    onlyfiles = [f for f in listdir(reflect_dir) if isfile(join(reflect_dir,f)) and f.endswith('_reflect.txt')]

    dataset=[]
    reflect_apk_names=[]
    reflect_class_counts = dict()

    MAX_FILES=sys.maxint
    file_limit = MAX_FILES if MAX_FILES <= len(onlyfiles) else len(onlyfiles)

    def create_reflect_feature_vector(f,reflect_dir):
        REFLECT_INVOKE_COUNT='reflect_invoke_count'
        NS_CONST_METHOD_COUNT='nonstring_constant_method_count'
        METHOD_NO_CLASS_COUNT='method_no_class_count'
        FULL_METHOD_COUNT='full_method_count'
        fv = dict()
        apk_name = f.replace('_reflect.txt','') + '.apk'
        logging.debug('apk name: {}'.format(apk_name))
    
        fp = open(join(reflect_dir,f),'r')
        part_method_counter=0
        for lineno, line in enumerate(fp, start=1):
            if line.strip(): # if the line is not empty
                logging.debug('found non-empty line in reflect file...')
                tokens = line.split(',')
                if len(tokens) == 2:
                    logging.debug('found 2 tokens on line...')
                    feature_name = 'ref#' + tokens[0]
                    feature_val = tokens[1]
                    fv[feature_name]=int(feature_val)
                    if not '.' in feature_name and not REFLECT_INVOKE_COUNT in feature_name and not NS_CONST_METHOD_COUNT in feature_name and not METHOD_NO_CLASS_COUNT in feature_name and not FULL_METHOD_COUNT in feature_name:
                        part_method_counter=part_method_counter+1
                else:
                    logging.warn('invalid number of token in line no. {} of file {}'.format(lineno,f))
        fv['part_method_counter']=part_method_counter
        logging.debug( 'part_method_counter: {}'.format(fv['part_method_counter']) )
        logging.debug( fv )
        logging.debug('')
        return apk_name,fv
    
    results = utils.parmap(functools.partial(create_reflect_feature_vector,reflect_dir=reflect_dir),onlyfiles[:file_limit])

    reflect_apk_names = [r[0] for r in results]
    dataset = [r[1] for r in results]

    X_reflect = v.fit_transform(dataset)
    for i,row in enumerate(X_reflect):
        logging.debug( '{},{}'.format(reflect_apk_names[i], row) )

    logging.debug("reflect feature names:")
    logging.debug(v.get_feature_names())
    
    logging.debug('reflect class counts:')
    logging.debug(reflect_class_counts)
    return reflect_apk_names, X_reflect, v.get_feature_names()

if __name__ == '__main__':
    main()
