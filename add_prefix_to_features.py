#!/usr/bin/env python

import h5py
import argparse
import time

def main():
    prog_start_time=time.time()
    parser = argparse.ArgumentParser(description='add a prefix to feature names within a given range')
    parser.add_argument('--hdf',help='HDF5 file to be modified')
    parser.add_argument('--start-idx',help='starting index of fnames to be modified',type=int)
    parser.add_argument('--end-idx',help='ending index of fnames to be modified',type=int)
    parser.add_argument('--prefix',help='the prefix to prepend to each feature name in the specified range')
    args = parser.parse_args()

    h5f = h5py.File(args.hdf)
    fnames = h5f['fnames']

    for i in range(args.start_idx, args.end_idx+1):
        fnames[i] = '{}{}'.format(args.prefix,fnames[i])

    h5f.close()
    print 'time for script to run: {}'.format(time.time()-prog_start_time)
    
if __name__ == '__main__':
      main()
