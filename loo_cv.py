#!/usr/bin/env python

import h5py
import argparse
import time
import utils
import sys
import numpy as np
import itertools
import matplotlib.pyplot as plt

from sklearn.model_selection import LeaveOneOut
from sklearn.metrics import precision_recall_fscore_support, confusion_matrix
from plot_confusion_matrix import plot_confusion_matrix

parser = argparse.ArgumentParser(description='perform a leave-one-out cross validation on an HDF5 file')
parser.add_argument('--hdf',help='input HDF5 file')
parser.add_argument('-c',metavar='C',default='d',help='select a classifier: k for nearest neighbor, d for decision tree (the default), s for SVM, l for linear SVC, r for logistic regression')
args = parser.parse_args()

loo = LeaveOneOut()

h5f = h5py.File(args.hdf)
X = h5f['X'][:]
y = h5f['y'][:]
apk_names = h5f['apk_names'][:]

dt = h5py.special_dtype(vlen=unicode)
y_results = np.empty((X.shape[0],),dtype=dt)
y_tgts = np.empty((X.shape[0],),dtype=dt)
apk_idx=0
for train_idx,test_idx in loo.split(X):
    start_time=time.time()
    print 'constructing classifier to test apk {}'.format(apk_names[apk_idx])
    sys.stdout.flush()
    X_train,X_test=X[train_idx],X[test_idx]
    y_train,y_test=y[train_idx],y[test_idx]

    clf = utils.select_classifier_and_fit(args.c,X_train,y_train)
    print 'time to construct classifier: {}'.format(time.time()-start_time)
    y_actual = clf.predict(X_test)

    y_results[apk_idx]=y_actual[0]
    y_tgts[apk_idx]=y_test[0]

    apk_idx=apk_idx+1

print 'y_tgts: {}'.format(y_tgts)
print 'y_results: {}'.format(y_results)
    
precision,recall,fscore,support=precision_recall_fscore_support(y_tgts,y_results,average='micro')
print 'precision,recall,fscore,support'
print '{},{},{},{}'.format(precision,recall,fscore,support)    
cm = confusion_matrix(y_tgts,y_results)

classes = sorted(set(y_tgts))
plt.figure()
plot_confusion_matrix(cm,classes,title='Confusion matrix, without normalization')
plt.show()
