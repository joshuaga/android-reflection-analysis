#!/usr/bin/env python

import unicodedata
import numpy as np
import pandas as pd
import arff
#import pydot
import os
import functools
import multiprocessing
import json
import sys
import time
import h5py
import chardet
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

from sklearn.metrics import classification_report,confusion_matrix,roc_curve,roc_auc_score
from plot_confusion_matrix import plot_confusion_matrix
#from sklearn import cross_validation
from sklearn.model_selection import cross_val_predict, train_test_split, KFold
from sklearn.externals.six import StringIO
from sklearn import metrics
from sklearn.utils.multiclass import unique_labels
from sklearn.feature_extraction import DictVectorizer
from sklearn.feature_selection import SelectFromModel

from sklearn import tree
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from sklearn.svm import LinearSVC
from sklearn.linear_model import LogisticRegression

from sklearn.metrics import precision_recall_curve, average_precision_score, auc
from sklearn.preprocessing import label_binarize

from scipy import interp

def create_timestr():
    timestr = time.strftime("%Y%m%d-%H%M%S")
    return timestr

def create_apk_indices_dict(apk_names):
    apk_name_indices=dict()
    for i,name in enumerate(apk_names):
        apk_name_indices[name]=i
    return apk_name_indices

def create_apk_index_to_name(apk_names):
    apk_idx_to_name=dict()
    for i,name in enumerate(apk_names):
        apk_idx_to_name[i]=name
    return apk_idx_to_name

def combine_feature_sets(apk_names1, apk_names2, X1, X2, y):
    new_apk_names = []
    X_combined = []
    y_combined = []
    for a_idx, apk_name1 in enumerate(apk_names1):
        for r_idx, apk_name2 in enumerate(apk_names2):
            if apk_name1.strip() == apk_name2.strip():
                #print apk_name1 + ' has reflect features'
                new_apk_names.append(apk_name1)
                #X_combined.append(X1[a_idx].tolist()).append(X2[r_idx])
                f = X1[a_idx].tolist()
                f.extend(X2[r_idx])
                X_combined.append(f)
                #f = np.append(X1[a_idx],X2[r_idx])
                #print f
                #X_combined.append(f)
                y_combined.append(y[a_idx])
    return np.array(new_apk_names),pd.lib.to_object_array(X_combined).astype(float),np.array(y_combined)

def extract_hdf_data(hdf_file):
    h5f=h5py.File(hdf_file,'r')
    X=h5f['X'][:]
    y=h5f['y'][:]
    apk_names=h5f['apk_names'][:]
    fnames=h5f['fnames'][:]

    #fnames=[]
    #for i,f in enumerate(h5f['fnames']):
    #    print chardet.detect(f.encode('latin-1'))
    #    print '{}: {}'.format(i,f.decode('utf8'))
    #    fnames.append(f.decode('utf8'))
    
    return apk_names,X,y,fnames

def store_hdf_data(new_hdf_filename,x,y,n,f):
    new_f = h5py.File(new_hdf_filename,'w')
    new_f.create_dataset('X',data=np.array(x),dtype=np.int)
    dt = h5py.special_dtype(vlen=unicode)
    new_f.create_dataset('y',data=np.array(y),dtype=dt)
    new_f.create_dataset('apk_names',data=np.array(n),dtype=dt)
    new_f.create_dataset('fnames',data=np.array(f),dtype=dt)
    new_f.close()

def extract_json_data(json_file):
    with open(json_file) as f:
        dataset = json.load(f)
        
    apk_names = []
    y = []
    for f in dataset:
        for n,v in f.iteritems():
            if n == 'apk_name':
                apk_names.append(v)
            if n == 'label':
                y.append(v)

    new_dataset=[]
    for f in dataset:
        f.pop('apk_name')
        f.pop('label')
        new_dataset.append(f)
    
    v = DictVectorizer()
    X=v.fit_transform(new_dataset).toarray()        
    return apk_names,X,y,v.get_feature_names()

def extract_arff_data(arff_file):
    dataset = arff.load(open(arff_file,'rb'))
    data = np.array(dataset['data'])
    apk_names = data[:,0]
    X=data[:,1:-1]
    y=data[:,-1]
    return apk_names,X,y,dataset

def convert_unicode_to_string(uni):
    return unicodedata.normalize('NFKD',uni).encode('ascii','ignore')

def print_classification_report(y_combined,predicted,dataset=None,inlabels=None):
    labels=[]
    if inlabels is not None:
        labels=inlabels
    elif dataset is not None:
        unicode_labels=dataset['attributes'][-1][1]
        for label in unicode_labels:
            s = convert_unicode_to_string(label)
            labels.append(s)
            
    report = None
    if labels:
        report = classification_report(y_combined,predicted,target_names=labels)
    else:
        report = classification_report(y_combined,predicted)

    print report

    
def print_confusion_matrix(y_test,y_pred):
    labels=unique_labels(y_test,y_pred)
    
    # Compute confusion matrix
    cm = confusion_matrix(y_test, y_pred,labels)
    np.set_printoptions(precision=2)
    print('Confusion matrix, without normalization')
    print(cm)

def write_tree_to_pdf(arff_file,dataset,clf):
    dot_data = StringIO()
    unicode_attributes=dataset['attributes'][1:-1]
    attributes=[]
    for attr in unicode_attributes:
        s = convert_unicode_to_string( attr[0] )
        attributes.append(s)
    unicode_labels=dataset['attributes'][-1][1]
    labels=[]
    for label in unicode_labels:
        s = convert_unicode_to_string( label )
        labels.append(s)
    tree.export_graphviz(clf, out_file=dot_data, max_depth=5, feature_names=attributes, class_names=labels, impurity=False, label='none')
    #graph = pydot.graph_from_dot_data(dot_data.getvalue())
    basename,ext=os.path.splitext(os.path.basename(arff_file))
    #graph.write_pdf(basename + '.pdf')

def get_basename(filename):
    basename,ext=os.path.splitext(os.path.basename(filename))
    return basename

def run_train_test_split(X,y):
    X_train,X_test,y_train,y_test=train_test_split(X,y,test_size=0.4,random_state=0)
    clf = tree.DecisionTreeClassifier()
    clf = clf.fit(X_train,y_train)
    print clf.score(X_test,y_test)

def select_classifier_and_fit(c,X_train,y_train):
    if c == 'd':
        # print 'selecting decision tree with LinearSVC feature selection'
        # sys.stdout.flush()
        # print 'original training X dataset shape: {}'.format(X_train.shape)
        # lsvc = LinearSVC(C=0.01,penalty="l1",dual=False).fit(X_train,y_train)
        # model = SelectFromModel(lsvc,prefit=True)
        # X_train_new = model.transform(X_train)
        # print 'new training X dataset shape: {}'.format(X_train_new.shape)
        # clf = tree.DecisionTreeClassifier().fit(X_train_new,y_train)
        print 'selecting decision tree'
        sys.stdout.flush()
        clf = tree.DecisionTreeClassifier().fit(X_train,y_train)
    elif c == 'k':
        print 'selecting k-NN'
        sys.stdout.flush()
        clf = KNeighborsClassifier().fit(X_train,y_train)
    elif c == 's':
        print 'selecting SVM'
        sys.stdout.flush()
        clf = SVC().fit(X_train,y_train)
    elif c == 'l':
        print 'selecting linear SVC'
        sys.stdout.flush()
        clf = LinearSVC(C=0.01,penalty="l1",dual=False).fit(X_train,y_train)
    else:
        print 'selecting decision tree'
        sys.stdout.flush()
        clf = tree.DecisionTreeClassifier().fit(X_train,y_train)
    return clf

def select_classifier(c):
    if c == 'd':
        print 'selecting decision tree'
        sys.stdout.flush()
        clf = tree.DecisionTreeClassifier()
    elif c == 'k':
        print 'selecting k-NN'
        sys.stdout.flush()
        clf = KNeighborsClassifier()
    elif c == 's':
        print 'selecting SVM'
        sys.stdout.flush()
        clf = SVC()
    elif c == 'l':
        print 'selecting linear SVC'
        sys.stdout.flush()
        clf = LinearSVC(C=0.01,penalty="l1",dual=False)
    elif c == 'r':
        print 'selecting logistic regression'
        sys.stdout.flush()
        clf = LogisticRegression(max_iter=1000)
    else:
        print 'selecting decision tree'
        sys.stdout.flush()
        clf = tree.DecisionTreeClassifier()
    return clf

def run_cv_with_prc_curve(clf, y, X, num_procs, numfolds=10,prc_filename='prc_curve.pdf'):
    kf = KFold(n_splits=numfolds)
    #base_fpr = np.linspace(0, 1, 101)

    #plt.figure(figsize=(5, 5))

    lw = 2
    #n_classes = y.shape[1]
    plt.clf()
    y_real = []
    y_proba = []
    for i, (train,test) in enumerate(kf.split(X)):
        model = clf.fit(X[train],y[train])
        y_score = model.predict_proba(X[test])

        precision = dict()
        recall = dict()
        average_precision = dict()        
        precision[i], recall[i], _ = precision_recall_curve(y[test],y_score[:,1],pos_label='Malware')
        #plt.plot(recall[i],precision[i],'b',alpha=0.15,lw=lw,color='navy',label='Precision-Recall curve')
        #average_precision[i] = average_precision_score(y[test],y_score[:,1])
        print 'AUC={0:0.2f}'.format(auc(recall[i],precision[i]))
        y_real.append(y[test])
        y_proba.append(y_score[:,1])
        
    y_real = np.concatenate(y_real)
    y_proba = np.concatenate(y_proba)
    precision, recall, _ = precision_recall_curve(y_real,y_proba,pos_label='Malware')
    label = 'Overall AUC={:.02f}'.format(auc(recall,precision))
    plt.plot(recall,precision,color='black',lw=lw,label=label)
    plt.xlabel('Recall')
    plt.ylabel('Precision')
    plt.ylim([0.0,1.05])
    plt.xlim([0.0,1.0])
    #plt.title('Precision-Recall example: AUC={0:0.2f}'.format(average_precision))]
    plt.legend(loc="lower left")
    plt.savefig(prc_filename)    

def run_cv_with_roc_curve(clf, y, X,num_procs,numfolds=10,roc_filename='roc_curve.pdf'):
    kf = KFold(n_splits=numfolds)

    tprs = []
    base_fpr = np.linspace(0, 1, 101)

    plt.figure(figsize=(5, 5))

    auc_scores=[]
    for i, (train,test) in enumerate(kf.split(X)):
        model = clf.fit(X[train],y[train])
        y_score = model.predict_proba(X[test])
        fpr, tpr, _ = roc_curve(y[test],y_score[:,1],pos_label='Malware')
        y_test_int = np.array( [0 if l == 'Benign' else 1 for l in y[test]] )
        auc = roc_auc_score(y_test_int,y_score[:,1])
        print '{} - auc: {}'.format(i,auc)
        auc_scores.append(auc)

        plt.plot(fpr, tpr, 'b', alpha=0.15)
        tpr = interp(base_fpr, fpr, tpr)
        tpr[0] = 0.0
        tprs.append(tpr)

    mean_auc = sum(auc_scores) / float( len(auc_scores) )
    print 'mean auc: {}'.format(mean_auc)
    tprs = np.array(tprs)
    mean_tprs = tprs.mean(axis=0)
    std = tprs.std(axis=0)

    tprs_upper = np.minimum(mean_tprs + std, 1)
    tprs_lower = mean_tprs - std


    plt.plot(base_fpr, mean_tprs, 'b')
    plt.fill_between(base_fpr, tprs_lower, tprs_upper, color='grey', alpha=0.3)

    plt.plot([0, 1], [0, 1],'r--')
    plt.xlim([-0.01, 1.01])
    plt.ylim([-0.01, 1.01])
    plt.ylabel('True Positive Rate')
    plt.xlabel('False Positive Rate')
    plt.axes().set_aspect('equal', 'datalim')
    plt.savefig(roc_filename)
    
    

def run_10fold_cv(clf, y, X,num_procs,numfolds=10,cm_title='Confusion matrix',cm_filename='cm.pdf',roc_filename='roc_curve.pdf'):    
    predicted=cross_val_predict(clf,X,y,cv=numfolds,n_jobs=num_procs)
    
    print 'accuracy: {}'.format(metrics.accuracy_score(y,predicted))
    print_classification_report(y,predicted)
    print_confusion_matrix(y,predicted)
    cm = confusion_matrix(y,predicted)
    classes = sorted(set(y))
    #plt.figure()
    #plot_confusion_matrix(cm,classes,title=cm_title)
    #plt.savefig(cm_filename)

    TP = cm[0][0]
    FP = cm[0][1]
    FN = cm[1][0]
    TN = cm[1][1]

    FPR = float(FP)/(float(FP)+float(TN))

    print 'false positive rate: {}'.format(FPR)

    fpr,tpr,thresholds=roc_curve(y,predicted,pos_label='Malware'.encode('utf8'))
    plt.plot(fpr,tpr)
    plt.savefig(roc_filename)

    #template = '{0:20}|{1:20}'
    #print template.format('Actual','Predicted')
    #for i,label in enumerate(y):
    #    print template.format(y[i],predicted[i])

def fun(f,q_in,q_out):
    while True:
        i,x = q_in.get()
        if i is None:
            break
        q_out.put((i,f(x)))

def parmap(f, X, nprocs = multiprocessing.cpu_count()):
    q_in   = multiprocessing.Queue(1)
    q_out  = multiprocessing.Queue()

    proc = [multiprocessing.Process(target=fun,args=(f,q_in,q_out)) for _ in range(nprocs)]
    for p in proc:
        p.daemon = True
        p.start()

    sent = [q_in.put((i,x)) for i,x in enumerate(X)]
    [q_in.put((None,None)) for _ in range(nprocs)]
    res = [q_out.get() for _ in range(len(sent))]

    [p.join() for p in proc]

    #return [x for i,x in sorted(res)]
    return [x for i,x in res]

def add_zero_features_to_missing(X_with_zeros_length, new_apk_names, prev_apk_names, y_combined, X_combined, y, X):
    start_time = time.time()
    new_apk_names_set=set(new_apk_names)
    #zero_features=np.zeros((1,len(X_with_zeros_length[0]))) # an array of all zeros the length of an X_with_zeros_length row
    MAX_ZERO_FEATURES=sys.maxint # limit the number of apks without reflection features
    apk_zero_count=0
    print 'time to perform simple initializations in add_zero_features_to_missing: {}'.format(time.time()-start_time)
    sys.stdout.flush()

    start_time = time.time()
    X_list = [[float(j) for j in i] for i in X.tolist()]
    X_new_list = X_combined.tolist()
    y_new_list = y_combined.tolist()
    zeros = [0]*len(X_with_zeros_length[0])
    new_apk_names_list=new_apk_names.tolist()
    print 'time to initialize tolist() block in add_zero_features_to_missing: {}'.format(time.time()-start_time)
    sys.stdout.flush()
    
    def extend_feature_vector_with_zeros((i,apk_name),X,y):
        X[i].extend(zeros)
        return apk_name,X[i],y[i]

    start_time=time.time()
    missing_list=[]
    for i,apk_name in enumerate(prev_apk_names):
        if not apk_name in new_apk_names_set:
            if apk_zero_count>=MAX_ZERO_FEATURES:
                break
            missing_list.append( (i,apk_name) )
            apk_zero_count+=1
    print 'time to populate missing_list: {}'.format(time.time()-start_time)
    sys.stdout.flush()

    start_time=time.time()
    results = parmap(functools.partial(extend_feature_vector_with_zeros,X=X_list,y=y),missing_list)
    unzipped_results = zip(*results)
    if len(unzipped_results) != 0:
        (missing_apk_names,missing_X,missing_y)=unzipped_results
        print 'time to extend missing_list with zero features: {}'.format(time.time()-start_time)
        sys.stdout.flush()

        start_time=time.time()
        new_apk_names_list.extend(missing_apk_names)
        X_new_list.extend(missing_X)
        y_new_list.extend(missing_y)
        print 'time to extend apk names list, new X list, and new y list: {}'.format(time.time()-start_time)
        sys.stdout.flush()
    else:
        print 'all APKs are accounted for, so skipping extending X, y, and apk_names'

    start_time=time.time()
    X_combined = pd.lib.to_object_array( [[float(j) for j in i] for i in X_new_list] ).astype(float)
    y_combined = np.array(y_new_list)
    new_apk_names = np.array(new_apk_names_list)
    print 'time to convert combined X, y, and new apk names to numpy arrays: {}'.format(time.time()-start_time)
    sys.stdout.flush()
    
    return new_apk_names, y_combined, X_combined

'''
  a is a two-dimensional numpy array
  p1 is the index to the right of the 1st cutoff point
  p2 is the index to the right of the 2nd cutoff point
  The cutoff points denote the first, second, and third ranges of columns. 
  The 1st cutoff point denotes the boundary between the first and second ranges.
  The 2nd cutoff point denotes the boundary between the second and third ranges.
'''
def swap_start_end_range_of_matrix(a,p1,p2):
    b = np.concatenate( (a[:,p2:],a[:,p1:p2]),axis=1 )
    c = np.concatenate( (b,a[:,0:2]), axis=1 )
    return c
