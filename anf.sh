#!/bin/bash
#$ -cwd
#$ -q seal,free*,pub*
#$ -o /share/seal/joshug4/ara_logs/
#$ -e /share/seal/joshug4/ara_logs/
#$ -pe openmp 16-64
# -l mem_free=200G
#$ -ckpt blcr

./attach_native_external_call_features.py $@
