#!/bin/bash
#$ -cwd
#$ -q seal,free*,pub*
#$ -o /share/seal/joshug4/ara_logs/
#$ -e /share/seal/joshug4/ara_logs/
#$ -ckpt blcr
#$ -pe openmp 16-64

./run_cv.py $@
