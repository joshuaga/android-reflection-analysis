#!/usr/bin/env python

import time
import argparse
import sys
import numpy as np

from sklearn import tree

def main():
    start_time = time.time()

    parser = argparse.ArgumentParser(description='train on one arff and test another arff')
    parser.add_argument('--train-names',help='file with training feature names')
    parser.add_argument('--train-x',help='file with training X dataset')
    parser.add_argument('--train-y',help='file with training Y dataset')
    parser.add_argument('--test-names',help='file with testing feature names')
    parser.add_argument('--test-x',help='file with testing X dataset')
    parser.add_argument('--test-y',help='file with testing Y dataset')
    args = parser.parse_args()

    print 'loading csv files'
    sys.stdout.flush()
    X_train = np.loadtxt(args.train_x,delimiter=',')
    y_train = np.loadtxt(args.train_y,dtype=np.dtype(np.str_),delimiter=',')
    X_test = np.loadtxt(args.test_x,delimiter=',')
    y_test = np.loadtxt(args.test_y,dtype=np.dtype(np.str_),delimiter=',')

    print 'running training and testing'
    clf = tree.DecisionTreeClassifier().fit(X_train,y_train)
    print clf.score(X_test,y_test)

    print 'total execution time: {}'.format(time.time()-start_time)

if __name__ == '__main__':
    main()
