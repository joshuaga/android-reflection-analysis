#!/bin/bash
#$ -cwd
#$ -pe openmp 4
#$ -q seal
#$ -j y
#$ -t 1-100
#$ -r y
#$ -ckpt restart
SEEDFILE=100_random_apps_list.txt
SEED=$(awk "NR==$SGE_TASK_ID" $SEEDFILE)
/share/seal/joshug4/workspace/android-reflection-analysis/ru.sh $SEED
