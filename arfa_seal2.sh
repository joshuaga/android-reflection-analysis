#!/bin/bash
#$ -cwd
#$ -q seal.q
#$ -j y
#$ -pe openmpi 4-32
# -ckpt blcr
# -l mem_size=256
# -l mem_free=32G

#module load java
./attach_reflect_features_to_arff.py $@
