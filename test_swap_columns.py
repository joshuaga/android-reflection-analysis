import unittest
import utils
import numpy as np

class TestSwapColumns(unittest.TestCase):

    def test_swap_columns(self):
        a = np.array([[10, 20, 30, 40, 50,60,70,80],[6,7,8,9,10,11,12,13]])
        result = utils.swap_start_end_range_of_matrix(a,2,4)
        expected = np.array([[50, 60, 70, 80, 30, 40, 10, 20],[10, 11, 12, 13,  8,  9,  6,  7]])
        np.testing.assert_array_equal(result, expected)

if __name__ == '__main__':
    unittest.main()
