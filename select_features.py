#!/usr/bin/env python

import argparse
import sys
import time
import numpy as np
import pandas as pd
import os

from sklearn.feature_selection import VarianceThreshold
from sklearn.svm import LinearSVC
from sklearn.pipeline import make_pipeline
from sklearn.model_selection import cross_val_score
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import chi2
from sklearn.feature_selection import SelectFromModel
from sklearn.model_selection import StratifiedKFold
from sklearn.model_selection import KFold
from sklearn.ensemble import ExtraTreesClassifier
from sklearn import tree

abs_start_time=time.time()
parser = argparse.ArgumentParser(description='script for conducting feature selection')

parser.add_argument('-x',help='dataset\'s X csv file',required=True)
parser.add_argument('-fn',help='dataset\'s feature names csv file',required=True)
parser.add_argument('-y',help='dataset\'s Y csv file',required=True)
parser.add_argument('-en',help='dataset\'s instance names csv file',required=True)
parser.add_argument('--fs-alg',metavar='C',default='t',help='feature selection algorithm: l for linear SVC; k for K best; t for a parallel forest of trees; and v for variance threshold')
parser.add_argument('--with-fs',dest='use_fs',action='store_true')
parser.add_argument('--no-fs',dest='use_fs',action='store_false')
parser.set_defaults(use_fs=True)
args = parser.parse_args()

print 'args: {}'.format(args)

print 'loading data'
sys.stdout.flush()
f = [line.rstrip('\n') for line in open(args.fn)]
start_time=time.time()
X = pd.read_csv(args.x,delimiter=',',header=None).values
y = np.loadtxt(args.y,dtype=np.dtype(np.str_),delimiter=',')
apk_names = open(args.en).readlines()

print 'time to load feature data: {}'.format(time.time()-start_time)
sys.stdout.flush()

start_time=time.time()
print 'shape before fs: {}'.format(X.shape)


def performFeatureSelection(y, X, f, fs_alg):
    start_time=time.time()
    print 'Performing feature selection..'
    sys.stdout.flush()
    lsvc = LinearSVC(C=0.01, penalty="l1", dual=False)
    sel = None
    if fs_alg == 'l':
        print 'using lsvc for feature selection'
        sel = SelectFromModel(lsvc)
    elif fs_alg == 'k':
        print 'using k best for feature selection'
        sel = SelectKBest(chi2, k=2000)
    elif fs_alg == 'v':
        print 'using various threshold for feature selection'
        sel = VarianceThreshold(threshold=(.8 * (1 - .8)))
    elif fs_alg == 't':
        print 'using parallel forest of trees'
        sel = SelectFromModel(ExtraTreesClassifier(n_estimators=250,n_jobs=-1))
    X_new = sel.fit_transform(X,y)

    print 'shape after fs: {}'.format(X_new.shape)
    sup = sel.get_support()
    print 'support: {}'.format(sup)

    selected_feature_names = [n for (i,n) in enumerate(f) if sup[i]]
    #print 'selected feature names:'
    #print selected_feature_names

    print 'number of features selected: {}'.format(len(selected_feature_names))
    assert len(selected_feature_names) == X_new.shape[1]

    print 'time to perform feature selection: {}'.format(time.time()-start_time)

    return X_new,selected_feature_names

do_fs = args.use_fs
X_new = None
new_feature_names = None
if do_fs:
    X_new,new_feature_names = performFeatureSelection(y, X, f, args.fs_alg)
else:
    X_new = X

if do_fs: 
    start_time=time.time()
    suffix='_reduced'
    timestr = time.strftime("%Y%m%d-%H%M%S")
    np_names_csv = os.path.join("res",os.path.basename(args.x) + suffix + '_in_' + timestr + '.csv')
    np_x_csv = os.path.join("res",os.path.basename(args.x) + suffix + '_x_' + timestr + '.csv')
    np_y_csv = os.path.join("res",os.path.basename(args.x) + suffix + '_y_' + timestr + '.csv')
    np_f_csv = os.path.join("res",os.path.basename(args.x) + suffix + '_fn_' + timestr + '.csv')

    np.savetxt(np_names_csv,apk_names,delimiter=',',fmt="%s")
    np.savetxt(np_x_csv,X_new.astype(np.float_),delimiter=',',fmt="%f")
    np.savetxt(np_y_csv,y,delimiter=',',fmt="%s")
    np.savetxt(np_f_csv,np.array(new_feature_names),delimiter=',',fmt="%s")
    print 'csv storage time: {}'.format(time.time()-start_time)


print 'Creating pipeline with feature selection and running cross-validation...'
sys.stdout.flush()
start_time = time.time()
#clf = make_pipeline(VarianceThreshold(threshold=(.8 * (1 - .8))),LinearSVC(C=0.01,penalty="l1",dual=False))
if len( set( y.tolist() ) ) == 2:
    print 'using linearSVC for classification'
    clf = LinearSVC(C=0.01,penalty="l1",dual=False)
else:
    print 'using decision tree for classification'
    clf = tree.DecisionTreeClassifier()
print 'X.shape: {}'.format(X.shape)
print 'y.shape: {}'.format(y.shape)
print 'cross-validation result:'
#skf = StratifiedKFold(n_splits=10)
kf = KFold(n_splits=10)
scores = cross_val_score(clf,X_new,y,cv=kf)
print 'time to run pipeline with cross-validation: {}'.format(time.time()-start_time)

print scores
print("Accuracy: %0.2f (+/- %0.2f)" % (scores.mean(), scores.std() * 2))
print 'total execution time: {}'.format(time.time()-abs_start_time)

