android-reflection-analysis includes (1) the reflection extractor of RevealDroid and (2) machine learning scripts for RevealDroid evaluation. To use the full features of RevealDroid, you'll also need the legacy code available at [this project](https://bitbucket.org/joshuaga/revealdroid).

To set up the dataset for RevealDroid from previous experiments, please follow the instructions [here](http://seal.ics.uci.edu/projects/revealdroid/index.html).

## Requirements ##

* the Soot framework
* scitkit-learn

A more detailed description of Java-based dependencies is described in our [legacy project](https://bitbucket.org/joshuaga/revealdroid).

## Scripts ##

Each script is further described if passed the -h option

* ```train_test__*_.py``` - each of these scripts runs a variety of training and splitting strategies for RevealDroid evaluation
* ```run_cv.py``` - performs cross-validation on RevealDroid datasets
* ```combine_features.py``` - combines features (e.g., reflection features with package API features)
* ```attach*features*.py```- attaches features (native or reflection) to other features in a dataset

* ```ru.sh``` - runs the refelection extractor

This repo also contains various *.sh grid engine scripts, which you can modify and use if you have a grid-engine cluster available to you.

## Datasets ##
Our datasets are stored in the ```res/``` directory as CSV files usable by sci-kit learn. Given its size (150GB in full size), we have created a minimal dataset for performing malware detection and family identification with RevealDroid, which is available [here](https://www.dropbox.com/sh/sm4ioj39kc30bfr/AACbb3YDsHo2U8zEFR3qSHiOa?dl=0) and totals about 8GB. All you need to do is copy the CSV files from that Dropbox directory to the ```android-reflection-analysis/res/``` directory. The full dataset has been removed from ```res/```, leaving only the symbolic links to the CSVs.