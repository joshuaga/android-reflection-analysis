#!/usr/bin/env python

import argparse
import time
import os
import logging
import numpy as np
import utils
import sys
import random
import h5py

from sklearn.feature_extraction import DictVectorizer

from scipy.sparse import coo_matrix
from scipy.sparse import hstack
from scipy.sparse import vstack

def main():
    prog_start_time = time.time()
    DEBUG=False

    parser = argparse.ArgumentParser(description="attach features to an existing dataset")
    parser.add_argument('--hdf1',help='hdf file with dataset that will be on the left')
    parser.add_argument('--hdf2',help='hdf file with dataset that will be on the right')
    parser.add_argument('--reduced',help='use reduced set, randomly selected using a random seed of 1',action='store_true',default=False)
    parser.add_argument('--reduced-size',help='set the size, in terms of number of apps, of reduced set',type=int,default=2000)
    args = parser.parse_args()
    print args

    timestr = time.strftime("%Y%m%d-%H%M%S")

    #logging.basicConfig(filename=logger_file,level=logging.DEBUG,format='%(levelname)s:%(name)s:%(funcName)s:%(message)s')
    print 'this script\'s name: ' + os.path.basename(__file__)
    #print 'will log to ' + logger_file
    sys.stdout.flush()

    #apk_names = np.loadtxt(args.n,dtype=np.dtype(np.str_),delimiter=',')
    load_prev_dataset_start_time=time.time()
    apk_names,X,y,feature_names=utils.extract_hdf_data(args.hdf1)
    X = coo_matrix(X)
    apk_names=apk_names.tolist()
    feature_names=feature_names.tolist()

    apk_names2,X2,y2,feature_names2=utils.extract_hdf_data(args.hdf2)
    X2 = coo_matrix(X2)
    apk_names2=apk_names2.tolist()
    feature_names2=feature_names2.tolist()
    
    #apk_names = open(args.n).readlines()
    #X = np.loadtxt(args.x,delimiter=',')
    #y = np.loadtxt(args.y,dtype=np.dtype(np.str_),delimiter=',')
    #feature_names=[line.rstrip('\n') for line in open(args.f)]
    print 'time to load datasets: {}'.format(time.time()-load_prev_dataset_start_time)
    sys.stdout.flush()

    apk_indices1 = utils.create_apk_indices_dict(apk_names)
    apk_indices2 = utils.create_apk_indices_dict(apk_names2)

    apk_idx_to_name1 = utils.create_apk_index_to_name(apk_names)
    apk_idx_to_name2 = utils.create_apk_index_to_name(apk_names2)

    # construct list of APKs in the 2nd dataset but not in the 1st
    # apk_names_missing_from_first = []
    # for apk_name in apk_names2:
    #     if not apk_name in apk_names:
    #         apk_names_missing_from_first.append(apk_name)

    X2_csr = X2.tocsr()
    X_right = coo_matrix( (len(apk_names),X2.shape[1]), dtype=np.int ).tolil()
    print 'X_right.shape: {}'.format(X_right.shape)

    # attach features from 2nd dataset for APKs in the first dataset
    start_time=time.time()
    row_count=0
    for apk_name in apk_names:
        if apk_name in apk_names2:
            row=X2_csr[ apk_indices2[apk_name] ,:].tocoo()
            for col_idx,val in zip(row.col,row.data):
                X_right[row_count,col_idx] = val
        row_count=row_count+1
    print 'time to attach X features from 2nd dataset to first: {}'.format(time.time()-start_time)
    sys.stdout.flush()

    if DEBUG:
        print 'X: '
        print X.toarray()
        print 'y: {}'.format(y)
        print 'X2: '
        print X2.toarray()
        print 'y2: {}'.format(y2)
        print 'X_right: '
        print X_right.toarray()
    
    print 'X.shape: {}'.format(X.shape)
    print 'X_right.shape: {}'.format(X_right.shape)
    X_lr = hstack([X,X_right])
    sys.stdout.flush()

    # add features for apks that exist in the 2nd dataset but do not exist in the first
    # start_time=time.time()
    # for apk_name2 in apk_names2:
    #     if apk_name2 not in apk_names:
    #         new_row = np.zeros(X_lr.shape[1],dtype=np.int)
    #         orig_row=X2_csr[apk_indices2[apk_name2],:].tocoo()
    #         for col_idx,val in zip(orig_row.col,orig_row.data):
    #             new_row[len(feature_names)+col_idx]=val
    #         X_lr = vstack( [X_lr,new_row] )

    # add labels from 2nd dataset whose apks are not in the first dataset
    # bottom_labels=[]
    # for i,apk_name2 in enumerate(apk_names2):
    #     if apk_name2 not in apk_names:
    #         bottom_labels.append(y2[i])
    # print 'time to add features and labels for apks in 2nd dataset that are not in the first: {}'.format(time.time()-start_time)
    # sys.stdout.flush()

    if DEBUG:
        print 'X_lr: \n{}'.format(X_lr.toarray())
    sys.stdout.flush()

    # create new APK name dataset
    # new_apk_names = [name.encode('utf8') for name in apk_names + apk_names_missing_from_first]
    # if DEBUG:
    #     print new_apk_names
    apk_names = [name.encode('utf8') for name in apk_names]

    # create new feature name dataset
    new_feature_names = [name.encode('utf8') for name in feature_names + feature_names2]
    if DEBUG:
        print new_feature_names

    # y_new = np.append(y,np.array(bottom_labels))
    # if DEBUG:
    #     print y_new

    reduced_size = args.reduced_size
    apk_names_reduced=[]
    X_reduced=[]
    y_reduced=[]
    X_csr = X.tocsr()
    if args.reduced:
        print 'using reduced set...'
        sys.stdout.flush()
        random.seed(1)
        selected_indices=random.sample(xrange(len(apk_names)),reduced_size)
        for i,apk_name in enumerate(apk_names):
            if i in selected_indices:
                apk_names_reduced.append(apk_names[i])
                X_reduced.append(X_csr[i])
                y_reduced.append(y[i])
        apk_names=apk_names_reduced
        X = np.array(X_reduced)
        y = np.array(y_reduced)
                
    #print apk_names
    #print X.toarray()
    #print y
    print 'X_lr.dtype: {}'.format(X_lr.dtype)
    sys.stdout.flush()

    print "storing new features as hdf files"
    sys.stdout.flush()
    store_start_time=time.time()
    new_hdf_filename = os.path.join("res",os.path.splitext(os.path.basename(args.hdf1))[0] + '_{}_'.format(os.path.splitext(os.path.basename(args.hdf2))[0]) + timestr + '.hdf')
    print 'new hdf filename: {}'.format(new_hdf_filename)

    #print 'type(new_apk_names):{}'.format(type(new_apk_names))
    utils.store_hdf_data(new_hdf_filename,X_lr.toarray(),y,apk_names,new_feature_names)
    print 'hdf storage time: {}'.format(time.time()-store_start_time)
    print('total execution time: %s' % (time.time() - prog_start_time))

if __name__ == '__main__':
    main()
