#!/bin/bash
#$ -cwd
#$ -q seal,free*,pub*
#$ -o /share/seal/joshug4/ara_logs/
#$ -e /share/seal/joshug4/ara_logs/
#$ -ckpt blcr

module load anaconda/2.7-4.3.1
./replace_hdf_labels.py $@
