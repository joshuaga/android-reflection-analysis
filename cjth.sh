#!/bin/bash
#$ -cwd
#$ -q seal,pub*
#$ -j y
#$ -pe openmp 4-32
# -ckpt blcr
# -l mem_size=256
#$ -l mem_free=32G

module load anaconda/2.7-4.3.1
./convert_json_to_hdf.py $@
