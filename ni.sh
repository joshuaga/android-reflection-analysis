#!/bin/bash
#$ -cwd
#$ -pe openmp 2-8
#$ -q free*,pub*
#$ -j y
# disabled -l mem_free=64G
# disabled -ckpt blcr
if [ -z ${ALT_HOME+x} ]; then
    ALT_HOME=$HOME
fi


export ANDROID_HOME=$ALT_HOME/android-sdks/platforms
export LD_LIBRARY_PATH=lib/

if [ -z ${1+x} ]; then
    echo no apk provided as input, so exiting
    exit 1
fi

echo "ALT_HOME="$ALT_HOME
java -classpath ../soot/libs/dexlib2-2.1.0-dev.jar:$ALT_HOME/workspace/pathexecutor/bin:$ALT_HOME/workspace/pathexecutor/lib/slf4j-api-1.7.6.jar:$ALT_HOME/workspace/pathexecutor/lib/logback-classic-1.1.2.jar:$ALT_HOME/workspace/soot-infoflow-android/bin:$ALT_HOME/workspace/soot/classes:$ALT_HOME/workspace/jasmin/classes:$ALT_HOME/workspace/jasmin/libs/java_cup.jar:$ALT_HOME/workspace/heros/bin:$ALT_HOME/workspace/heros/guava-18.0.jar:$ALT_HOME/workspace/heros/slf4j-api-1.7.5.jar:$ALT_HOME/workspace/heros/junit.jar:$ALT_HOME/workspace/heros/org.hamcrest.core_1.3.0.jar:$ALT_HOME/workspace/soot/libs/polyglot.jar:$ALT_HOME/workspace/soot/libs/AXMLPrinter2.jar:$ALT_HOME/workspace/soot/libs/hamcrest-all-1.3.jar:$ALT_HOME/workspace/soot/libs/junit-4.11.jar:$ALT_HOME/workspace/soot/libs/util-2.0.3-dev.jar:$ALT_HOME/workspace/soot/libs/asm-debug-all-5.0.3.jar:$ALT_HOME/workspace/soot-infoflow-android/lib/polyglot.jar:$ALT_HOME/workspace/soot-infoflow-android/lib/AXMLPrinter2.jar:$ALT_HOME/workspace/soot-infoflow-android/lib/commons-io-2.4.jar:$ALT_HOME/workspace/soot-infoflow/bin:$ALT_HOME/workspace/soot-infoflow/lib/cos.jar:$ALT_HOME/workspace/soot-infoflow/lib/j2ee.jar:$ALT_HOME/workspace/soot-infoflow/lib/slf4j-api-1.7.5.jar:$ALT_HOME/workspace/soot-infoflow/lib/slf4j-simple-1.7.5.jar:$ALT_HOME/workspace/soot-infoflow-android/lib/axml-2.0.jar:$ALT_HOME/workspace/handleflowdroid/bin:$ALT_HOME/workspace/SootUtil/bin:$ALT_HOME/workspace/SootUtil/lib/jarchivelib-0.5.0-with-dependencies.jar:$ALT_HOME/workspace/SootUtil/lib/jsoup-1.7.3.jar:$ALT_HOME/workspace/SootUtil/lib/axml-1.0.jar:$ALT_HOME/workspace/SootUtil/lib/soot-infoflow-android.jar:$ALT_HOME/workspace/SootUtil/lib/soot-infoflow.jar:$ALT_HOME/workspace/SootUtil/lib/soot.jar:$ALT_HOME/workspace/pathexecutor/lib/javatuples-1.2.jar:$ALT_HOME/workspace/pathexecutor/lib/jgrapht-jdk1.6.jar:$ALT_HOME/workspace/pathexecutor/lib/AXMLPrinter2.jar:$ALT_HOME/workspace/pathexecutor/lib/apk-parser-all.jar:$ALT_HOME/workspace/pathexecutor/lib/logback-core-1.1.2.jar:$ALT_HOME/workspace/pathexecutor/lib/com.microsoft.z3.jar:$ALT_HOME/workspace/TrimDroid/bin:$ALT_HOME/workspace/TrimDroid/lib/alloy4.2.jar:$ALT_HOME/workspace/TrimDroid/lib/commons-io-2.4.jar:lib/android.jar:$ALT_HOME/workspace/seal-utils/bin edu.uci.seal.cases.analyses.NewInstanceReflectTransformer $1
