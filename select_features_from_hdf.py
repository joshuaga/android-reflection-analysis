#!/usr/bin/env python

from sklearn.ensemble import ExtraTreesClassifier
from sklearn.datasets import load_iris
from sklearn.feature_selection import SelectFromModel
from sklearn.svm import LinearSVC
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import chi2
from sklearn.feature_selection import VarianceThreshold

import numpy as np
import h5py
import argparse
import time
import os
import sys

def performFeatureSelection(y, X, f, fs_alg):
    start_time=time.time()
    print 'Performing feature selection..'
    sys.stdout.flush()
    lsvc = LinearSVC(C=0.01, penalty="l1", dual=False)
    sel = None
    if fs_alg == 'l':
        print 'using lsvc for feature selection'
        sel = SelectFromModel(lsvc)
    elif fs_alg == 'k':
        print 'using k best for feature selection'
        sel = SelectKBest(chi2, k=10000)
    elif fs_alg == 'v':
        print 'using various threshold for feature selection'
        sel = VarianceThreshold(threshold=(.8 * (1 - .8)))
    elif fs_alg == 't':
        print 'using parallel forest of trees'
        sel = SelectFromModel(ExtraTreesClassifier(n_estimators=250,n_jobs=-1))
    X_new = sel.fit_transform(X,y)

    print 'shape after fs: {}'.format(X_new.shape)
    sup = sel.get_support()
    print 'support: {}'.format(sup)

    selected_feature_names = [n for (i,n) in enumerate(f) if sup[i]]
    #print 'selected feature names:'
    #print selected_feature_names

    print 'number of features selected: {}'.format(len(selected_feature_names))
    assert len(selected_feature_names) == X_new.shape[1]

    print 'time to perform feature selection: {}'.format(time.time()-start_time)

    return X_new,selected_feature_names


parser = argparse.ArgumentParser(description='script for conducting feature selection on hdf5 file')

parser.add_argument('-f',help='HDF5 input file',required=True)
parser.add_argument('--fs-alg',metavar='C',default='t',help='feature selection algorithm: l for linear SVC; k for K best; t for a parallel forest of trees; and v for variance threshold')
parser.add_argument('--with-fs',dest='use_fs',action='store_true')
args = parser.parse_args()

start_time=time.time()
print 'loading data'
h5f = h5py.File(args.f,'r')
X=h5f['X'][:]
y=h5f['y'][:]
apk_names=h5f['apk_names'][:]
fnames=h5f['fnames'][:]
print 'time to load {}: {}'.format(args.f,time.time()-start_time)
sys.stdout.flush()

print 'fnames type: {}'.format(type(fnames))
#print 'fnames: {}'.format([n.decode() for n in fnames])

print 'X.shape: {}'.format(X.shape)
print 'y.shape: {}'.format(y.shape)
#print 'apk_names: {}'.format(apk_names)
sys.stdout.flush()

X_new,sel_features=performFeatureSelection(y,X,fnames,args.fs_alg)

sel_features = [x.encode('utf-8') for x in sel_features]

timestr = time.strftime("%Y%m%d-%H%M%S")

start_time=time.time()
in_basename = os.path.splitext(args.f)[0]
reduced_filename=in_basename + '_' + timestr + '_reduced.hdf'
new_f = h5py.File(reduced_filename,'w')
new_f.create_dataset('X',data=X_new,dtype=int)
dt = h5py.special_dtype(vlen=unicode)
new_f.create_dataset('y',data=np.array(y),dtype=dt)
new_f.create_dataset('apk_names',data=np.array(apk_names),dtype=dt)
new_f.create_dataset('fnames',data=np.array(sel_features),dtype=dt)
new_f.close()
print 'time to store reduced dataset: {}'.format(time.time()-start_time)
sys.stdout.flush()
