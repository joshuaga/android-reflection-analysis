#!/usr/bin/env python

import argparse
from os import listdir
from os.path import isfile, join
from sklearn.feature_extraction import DictVectorizer

v = DictVectorizer(sparse=False)

parser = argparse.ArgumentParser(description="convert a directory of log files to a feature vector")
parser.add_argument('dir',help='directory with log files')
args = parser.parse_args()

#fmf = open('android-22-method_names.txt','r')
#full_method_names = [m for m in fmf]

onlyfiles = [f for f in listdir(args.dir) if isfile(join(args.dir,f)) and f.startswith('edu.uci.seal.cases.analyses.ReflectUsageTransformer-') and f.endswith('.log')]

REFLECT_INVOKE_COUNT='reflect_invoke_count'
NS_CONST_METHOD_COUNT='nonstring_constant_method_count'
METHOD_NO_CLASS_COUNT='method_no_class_count'
FULL_METHOD_COUNT='full_method_count'

dataset=[]
apknames=[]
for f in onlyfiles:
    fv = dict()
    parsing_full_method_counts = False
    parsing_part_method_counts = False
    apk_name = f.replace('edu.uci.seal.cases.analyses.ReflectUsageTransformer-','')
    apk_name = apk_name.replace('-main.log','')
    print apk_name
    #fv['apk_name']=apk_name
    apknames.append(apk_name)
    fp = open(join(args.dir,f),'r')
    part_method_counter=0
    for line in fp:
        if 'reflect_invoke_count:' in line:
            fv[REFLECT_INVOKE_COUNT] = int(line.strip().split(' ')[-1])
            print REFLECT_INVOKE_COUNT + ': {}'.format(fv[REFLECT_INVOKE_COUNT])
        if 'nonstring_constant_method_count:' in line:
            fv[NS_CONST_METHOD_COUNT] = int(line.strip().split(' ')[-1])
            print NS_CONST_METHOD_COUNT + ': {}'.format(fv[NS_CONST_METHOD_COUNT])
        if 'method_no_class_count: ' in line:
            fv[METHOD_NO_CLASS_COUNT] = int(line.strip().split(' ')[-1])
            print METHOD_NO_CLASS_COUNT + ': {}'.format(fv[METHOD_NO_CLASS_COUNT])
        if 'full_method_count: ' in line:
            fv[FULL_METHOD_COUNT] = int(line.strip().split(' ')[-1])
            print FULL_METHOD_COUNT + ': {}'.format(fv[FULL_METHOD_COUNT])
            
        if 'Partial method counts:' in line:
            parsing_full_method_counts = False
            parsing_part_method_counts = True
            
        if parsing_full_method_counts and not parsing_part_method_counts:
            tokens = line.split('|')[-1].split(':')
            full_method_name = tokens[-2].strip()
            count = tokens[-1].strip()
            fv[full_method_name] = int(count)
            print full_method_name + ': {}'.format(fv[full_method_name])

        if parsing_part_method_counts:
            if not 'total runtime' in line and not 'Reached end' in line and not 'Partial method counts' in line:
                part_method_counter=part_method_counter+1
                tokens = line.split('|')[-1].split(':')
                part_method_name = tokens[-2].strip()
                part_count = tokens[-1].strip()
                fv[part_method_name]=int(part_count)
                print part_method_name + ': {}'.format(fv[part_method_name])
        if 'Full method counts:' in line:
            parsing_full_method_counts = True
    fv['part_method_counter']=part_method_counter
    print 'part_method_counter: {}'.format(fv['part_method_counter'])
    print fv
    dataset.append(fv)
    print ''

X = v.fit_transform(dataset)
for i,row in enumerate(X):
    print apknames[i], row
print v.get_feature_names()
