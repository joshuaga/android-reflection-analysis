#!/usr/bin/env python

import h5py
import argparse
import numpy as np

parser = argparse.ArgumentParser(description='removes whitespace from HDF5 file containing fnames dataset')
parser.add_argument('hdf',help='HDF file')
args = parser.parse_args()

h5f = h5py.File(args.hdf)
fnames = h5f['fnames']
fnames = [n.strip().encode('utf8') for n in fnames]

del h5f['fnames']
h5f.create_dataset('fnames',data=fnames,dtype=h5py.special_dtype(vlen=unicode))
h5f.close()


