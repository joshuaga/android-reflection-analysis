#!/bin/bash
#$ -cwd
#$ -q seal,free*,pub*
#$ -o /share/seal/joshug4/ara_logs/
#$ -e /share/seal/joshug4/ara_logs/
#$ -pe openmp 8-16
#$ -l mem_size=256
#$ -ckpt blcr

./add_samples.py $@
