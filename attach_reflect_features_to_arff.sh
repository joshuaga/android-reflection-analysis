#!/bin/bash
#$ -cwd
#$ -q seal,pub*
#$ -o /share/seal/joshug4/ara_logs/
#$ -e /share/seal/joshug4/ara_logs/
#$ -pe openmp 4-32
# -ckpt blcr
# -l mem_size=256
#$ -l mem_free=64G

module load anaconda/2.7-4.3.1
./attach_reflect_features_to_arff.py $@
