#!/usr/bin/env python

import h5py
import argparse
import os
import matplotlib.pyplot as plt
import matplotlib
import pandas as pd
import numpy as np
from collections import Counter
from operator import itemgetter

parser = argparse.ArgumentParser(description='plot histogram of y dataset in HDF5 file')
parser.add_argument('--hdf',help='input HDF5 file')
parser.add_argument('--filter-infreq',help='filter infrequent labels',action='store_true')
args = parser.parse_args()

print args

h5f = h5py.File(args.hdf,'r')
y = h5f['y'][:]
print y

label_counts = Counter(y)
label_counts_new=dict()

if args.filter_infreq:
    for label,counts in label_counts.items():
        if counts >= 100:
            label_counts_new[label]=counts
else:
    label_counts_new=label_counts
            

#df = pd.DataFrame.from_dict(label_counts_new,orient='index')
#df.plot(kind='bar',sort_columns=True,logy=True)

c = label_counts_new.items()
c.sort(key=itemgetter(0))
labels, values = zip(*c)
indexes = np.arange(len(labels))
width = 1
#plt.figure(figsize=(8,4),dpi=300)
print 'number of labels: {}'.format(len(labels))

fig, ax = plt.subplots()
fig.set_size_inches(20,17)
fig.set_dpi(300)
#for i, v in enumerate(label_counts_new.values()):
#        ax.text(v + 3, i + .25, str(v), color='blue', fontweight='bold')
ax.get_yaxis().set_major_formatter(
        matplotlib.ticker.FuncFormatter(lambda x, p: format(int(x), ',')))
rects = plt.bar(indexes, values, width)
plt.xticks(indexes + width*-.01, labels, rotation='vertical')
plt.xlabel('Families')
plt.ylabel('Frequency')

def autolabel(rects):
    """
    Attach a text label above each bar displaying its height
    """
    for rect in rects:
        height = rect.get_height()
        ax.text(rect.get_x() + rect.get_width()/2., 1.00*height,
                "{:,}".format(int(height)),
                ha='center', va='bottom')
autolabel(rects)

#plt.show()

# print 'creating new figure'
# plt.figure()
# print 'creating new histogram using matplotlib'
# plt.hist(y)

basename = os.path.splitext(os.path.basename(args.hdf))[0]
new_filename='{}_histogram.pdf'.format(basename)
print 'storing histogram as file {}'.format(new_filename)
plt.savefig(new_filename)
