#!/usr/bin/env python

import argparse
import h5py
import utils
import numpy as np
import time

prog_start_time=time.time()

parser = argparse.ArgumentParser(description='filter dataset using list of hashes or apk names')
parser.add_argument('--hdf',help='HDF5 file with dataset')
parser.add_argument('--filter-file',help='file with list of hashes or APK names')
args = parser.parse_args()
print args

start_time=time.time()
h5f = h5py.File(args.hdf,'r')
apk_names = h5f['apk_names'][:]
y = h5f['y'][:]
X = h5f['X']
filter_file = open(args.filter_file,'r')
hashes = set()
for line in filter_file:
    line = line.strip()
    if line:
        hashes.add(line)
print 'time to load input data: {}'.format(time.time()-start_time)

start_time=time.time()
apk_names_new=[]
y_new=[]
apk_indices=[]
for i,apk_name in enumerate(apk_names):
    if apk_name in hashes:
        apk_names_new.append(apk_name.encode('utf8'))
        y_new.append(y[i].encode('utf8'))
        apk_indices.append(i)
X_new = X[np.array(apk_indices),:]
print 'time to create new dataset: {}'.format(time.time()-start_time)

start_time=time.time()
hdf_basename = utils.get_basename(args.hdf)
filter_basename = utils.get_basename(args.filter_file)
filename_new = 'res/{}_{}.hdf'.format(hdf_basename,filter_basename)
dt = h5py.special_dtype(vlen=unicode)
h5f_new = h5py.File(filename_new)
h5f_new.create_dataset('apk_names',data=np.array(apk_names_new),dtype=dt)
h5f_new.create_dataset('y',data=np.array(y_new),dtype=dt)
h5f_new.create_dataset('X',data=X_new,dtype=np.int)
h5f.copy('fnames',h5f_new)
h5f.close()
print 'time to store new dataset: {}'.format(time.time()-start_time)

print 'total execution time: {}'.format(time.time()-start_time)
