../#!/bin/bash
#$ -cwd
#$ -pe openmp 1-4
#$ -q free*,pub*
#$ -j y
#$ -t 1-55386
#$ -r y
#$ -ckpt restart
SEEDFILE=pvd_apps_list.txt
SEED=$(awk "NR==$SGE_TASK_ID" $SEEDFILE)
/share/seal/joshug4/workspace/android-reflection-analysis/ru.sh $SEED
