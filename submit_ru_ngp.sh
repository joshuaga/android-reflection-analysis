../#!/bin/bash
#$ -cwd
#$ -pe openmp 1-4
#$ -q free*,pub*
#$ -j y
#$ -t 1-2610
#$ -r y
#$ -ckpt restart
SEEDFILE=ngp_apps_list.txt
SEED=$(awk "NR==$SGE_TASK_ID" $SEEDFILE)
/share/seal/joshug4/workspace/android-reflection-analysis/ru.sh $SEED
