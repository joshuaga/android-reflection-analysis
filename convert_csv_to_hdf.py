#!/usr/bin/env python

import argparse
import pandas as pd
import h5py
import time
import sys
import numpy as np
import utils
import codecs
import csv

from os.path import basename
from os.path import splitext

parser = argparse.ArgumentParser(description='script for converting a CSV file to an HDF5 file')
parser.add_argument('-x',help='dataset\'s X csv file')
parser.add_argument('-y',help='dataset\'s y csv file')
parser.add_argument('-en',help='dataset\'s instance names file')
parser.add_argument('-fn',help='dataset\'s feature names file')
args = parser.parse_args()
print args

x_bn = splitext(args.x)[0]

start_time=time.time()
apk_names = [line.rstrip('\n') for line in open(args.en) if line.strip()]
X = pd.read_csv(args.x,delimiter=',',header=None).values
y = [line.rstrip('\n') for line in open(args.y)]
feature_names=[line.encode('utf8') for line in open(args.fn)]
print 'time to load CSV files: {}'.format(time.time()-start_time)

start_time=time.time()
new_f = h5py.File( '{}.hdf'.format(x_bn),'w' )
new_f.create_dataset('X',data=X,dtype=int)
dt = h5py.special_dtype(vlen=unicode)
new_f.create_dataset('y',data=np.array(y),dtype=dt)
new_f.create_dataset('apk_names',data=np.array(apk_names),dtype=dt)
new_f.create_dataset('fnames',data=np.array(feature_names),dtype=dt)
new_f.close()

print 'time to store to HDF5: {}'.format(time.time()-start_time)
sys.stdout.flush()

