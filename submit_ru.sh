#!/bin/bash
#$ -cwd
#disabled -pe openmp 2
#$ -q pub*
#$ -j y
indir=$1
for apk in $indir/*.apk
do
    echo Submitting job for ru.sh $apk
    qsub ru.sh $apk
done