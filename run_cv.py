#!/usr/bin/env python

import time
import argparse
import numpy as np
import utils
import sys
import pandas as pd
import h5py
import os

#from sklearn.feature_selection import SelectFromModel
#from sklearn.svm import LinearSVC

def main():
    start_time = time.time()

    parser = argparse.ArgumentParser(description='run cross-validation from CSV files')
    parser.add_argument('-x',help='file with X dataset')
    parser.add_argument('-y',help='file with Y dataset')
    parser.add_argument('--hdf',help='file with HDF dataset')
    parser.add_argument('-f',help='number of folds for cross-validation',type=int,default=10)
    parser.add_argument('-s',metavar='S',type=int,help='select only the first S instances for cross-validation')
    parser.add_argument('-l',metavar='S',type=int,help='select only the last S instances for cross-validation')
    parser.add_argument('-c',metavar='C',default='d',help='select a classifier: k for nearest neighbor, d for decision tree (the default), s for SVM, l for linear SVC, r for logistic regression')
    parser.add_argument('--num-procs',help='number of processors to use',type=int,required=True)
    args = parser.parse_args()

    print args

    if not ((args.x and args.y) or args.hdf):
        raise StandardError('You must supply either an X csv and y csv, or an HDF file')

    X=None
    y=None
    if args.x and args.y:
        print 'loading csv files'
        print args.x
        print args.y
        sys.stdout.flush()
        sec_time=time.time()
        #X = np.loadtxt(args.x,delimiter=',')

        X = pd.read_csv(args.x,delimiter=',',header=None).values
        y = np.loadtxt(args.y,dtype=np.dtype(np.str_),delimiter=',')
        print 'time to load csv files: {}'.format(time.time()-sec_time)
        sys.stdout.flush()
    
    if args.hdf:
        print 'loading HDF file'
        sys.stdout.flush()
        
        start_time=time.time()
        h5f = h5py.File(args.hdf,'r')
        X=h5f['X'][:]
        y=h5f['y'][:]
        print 'time to load HDF file: {}'.format(time.time()-start_time)

    if args.s and args.y:
        raise StandardError('You can\'t supply both -s and -l, please choose 1')

    if args.s:
        X = X[:args.s]
        y = y[:args.s]
    if args.l:
        X = X[-args.l:]
        y = y[-args.l:]

    print "running {}-fold cv".format(args.f)
    sys.stdout.flush()
    clf = utils.select_classifier(args.c)
    #print 'original training X dataset shape: {}'.format(X.shape)
    #lsvc = LinearSVC(C=0.01,penalty="l1",dual=False).fit(X,y)
    #model = SelectFromModel(lsvc,prefit=True)
    #X_new = model.transform(X)
    #print 'new training X dataset shape: {}'.format(X_new.shape)
    basename = os.path.splitext(os.path.basename(args.hdf))[0]
    cm_filename='res/{}_cm.pdf'.format(basename)
    roc_filename='res/{}_{}-clf_roc.pdf'.format(basename,args.c)
    prc_filename='res/{}_{}-clf_prc.pdf'.format(basename,args.c)
    #utils.run_10fold_cv(clf,y,X,args.num_procs,numfolds=args.f,cm_filename=cm_filename,cm_title='Confusion Matrix',roc_filename=roc_filename)
    #utils.run_cv_with_roc_curve(clf,y,X,args.num_procs,numfolds=args.f,roc_filename=roc_filename)
    utils.run_cv_with_prc_curve(clf,y,X,args.num_procs,numfolds=args.f,prc_filename=prc_filename)

    print 'total execution time: {}'.format(time.time()-start_time)

if __name__ == '__main__':
    main()
