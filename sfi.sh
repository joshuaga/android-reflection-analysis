#!/bin/bash
#$ -cwd
#$ -pe openmp 16-64
#$ -q seal,free*,pub*
#$ -o /share/seal/joshug4/ara_logs/
#$ -e /share/seal/joshug4/ara_logs/
#$ -ckpt blcr

module load anaconda/2.7-4.3.1
./select_features_incrementally.py $@
