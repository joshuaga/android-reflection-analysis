#!/usr/bin/env python

import argparse
import time
import os
import logging
import numpy as np
import utils
import sys
import random

from sklearn.feature_extraction import DictVectorizer

def main():
    start_time = time.time()

    parser = argparse.ArgumentParser(description="attach features to an existing dataset")
    parser.add_argument('--hdf1',help='hdf file with dataset that will be on the left')
    parser.add_argument('--hdf2',help='hdf file with dataset that will be on the right')
    parser.add_argument('--reduced',help='use reduced set, randomly selected using a random seed of 1',action='store_true',default=False)
    parser.add_argument('--reduced-size',help='set the size, in terms of number of apps, of reduced set',type=int,default=2000)
    args = parser.parse_args()
    print args

    timestr = time.strftime("%Y%m%d-%H%M%S")

    #logging.basicConfig(filename=logger_file,level=logging.DEBUG,format='%(levelname)s:%(name)s:%(funcName)s:%(message)s')
    print 'this script\'s name: ' + os.path.basename(__file__)
    #print 'will log to ' + logger_file
    sys.stdout.flush()

    #apk_names = np.loadtxt(args.n,dtype=np.dtype(np.str_),delimiter=',')
    load_prev_dataset_start_time=time.time()
    apk_names,X,y,feature_names=utils.extract_hdf_data(args.hdf1)
    apk_names=apk_names.tolist()
    feature_names=feature_names.tolist()

    apk_names2,X2,y2,feature_names2=utils.extract_hdf_data(args.hdf2)
    apk_names2=apk_names2.tolist()
    feature_names2=feature_names2.tolist()
    
    #apk_names = open(args.n).readlines()
    #X = np.loadtxt(args.x,delimiter=',')
    #y = np.loadtxt(args.y,dtype=np.dtype(np.str_),delimiter=',')
    #feature_names=[line.rstrip('\n') for line in open(args.f)]
    print 'time to load datasets: {}'.format(time.time()-load_prev_dataset_start_time)
    sys.stdout.flush()
    
    use_reduced_set = args.reduced
    reduced_size = args.reduced_size
    apk_names_reduced=[]
    X_reduced=[]
    y_reduced=[]
    if use_reduced_set:
        print 'using reduced set...'
        sys.stdout.flush()
        random.seed(1)
        selected_indices=random.sample(xrange(len(apk_names)),reduced_size)
        for i,apk_name in enumerate(apk_names):
            if i in selected_indices:
                apk_names_reduced.append(apk_names[i])
                X_reduced.append(X[i])
                y_reduced.append(y[i])
        apk_names=apk_names_reduced
        X = np.array(X_reduced)
        y = np.array(y_reduced)
                
    #print apk_names
    print X
    print y
    sys.stdout.flush()

    print "combining previous features with attachable features"
    sys.stdout.flush()
    combine_nonzero_features_start_time=time.time()
    new_apk_names,X_new,y_new=utils.combine_feature_sets(apk_names,apk_names2,X,X2,y)
    new_feature_names=feature_names[:]
    new_feature_names.extend(feature_names2)
    print 'time to combine nonzero features with previous features: {}'.format(time.time()-combine_nonzero_features_start_time)

    print 'adding zero features for instances without new features'
    sys.stdout.flush()
    zf_start_time = time.time()
    new_apk_names,y_new,X_new=utils.add_zero_features_to_missing(X2,new_apk_names,apk_names,y_new,X_new,y,X)
    print 'time to add zero features: {}'.format(time.time()-zf_start_time)
    sys.stdout.flush()

    print "storing new features as hdf files"
    sys.stdout.flush()
    store_start_time=time.time()
    new_hdf_filename = os.path.join("res",os.path.splitext(os.path.basename(args.hdf1))[0] + '_{}_'.format(os.path.splitext(os.path.basename(args.hdf2))[0]) + timestr + '.hdf')
    print 'new hdf filename: {}'.format(new_hdf_filename)

    new_apk_names=[n.encode('utf8') for n in new_apk_names]
    new_feature_names=[fn.encode('utf8') for fn in new_feature_names]
    y_new=[l.encode('utf8') for l in y_new]
    utils.store_hdf_data(new_hdf_filename,X_new,y_new,new_apk_names,new_feature_names)
    print 'hdf storage time: {}'.format(time.time()-store_start_time)
    print('total execution time: %s' % (time.time() - start_time))

if __name__ == '__main__':
    main()
