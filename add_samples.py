#!/usr/bin/env python

import h5py
import argparse
import numpy as np
import time
import sys

from scipy.sparse import lil_matrix,coo_matrix,csr_matrix

def main():
    prog_start_time=time.time()
    parser = argparse.ArgumentParser(description='added samples to an HDF file from an existing HDF file')
    parser.add_argument('--hdf1',help='HDF5 file to be extended. Its feature space will not change.')
    parser.add_argument('--hdf2',help='HDF5 file with samples to be added to the other HDF5 file')
    parser.add_argument('--new-hdf',help='new HDF5 file with added samples')
    args = parser.parse_args()
    print args
    
    start_time=time.time()
    h5f1 = h5py.File(args.hdf1,'r')
    h5f2 = h5py.File(args.hdf2,'r')
    h5f_new = h5py.File(args.new_hdf,'w')
    
    X1 = h5f1['X'][:]
    X2 = h5f2['X']
    
    y1 = h5f1['y'][:]
    y2 = h5f2['y'][:]
    y_new = np.hstack((y1,y2))
    
    fnames1 = h5f1['fnames'][:] 
    fnames2 = h5f2['fnames'][:]
    
    apk_names1 = h5f1['apk_names']
    apk_names2 = h5f2['apk_names']
    apk_names_new = np.hstack((apk_names1,apk_names2))
    print 'time to load data from HDF files: {}'.format(time.time()-start_time)
    sys.stdout.flush()
    
    start_time=time.time()
    fname2_to_index=dict()
    for i,n2 in enumerate(fnames2):
        fname2_to_index[n2]=i

    print 'creating new X dataset with all zeros, converting to that dataset and X2 to sparse matrices, and create fn1_in_fn2 list'
    sys.stdout.flush()
    phase_start_time=time.time()
    X_new = lil_matrix( np.zeros( (X2.shape[0]+X1.shape[0],X1.shape[1]), dtype=np.int ) )
    X2 = coo_matrix( X2 ).tocsr()
    X1 = coo_matrix( X1 )
    fn1_in_fn2=[]
    for i,fname in enumerate(fnames1):
        if fname in fname2_to_index.keys():
            fn1_in_fn2.append( (i,fname) )
    print 'time to complete this phase: {}'.format(time.time()-phase_start_time)
    sys.stdout.flush()

    print 'modifying X dataset'
    sys.stdout.flush()
    start_row=X1.shape[0]
    inc_size=100
    modx_start_time=time.time()
    x2_start_time=time.time()
    print 'populating X2 portion of X_new'
    sys.stdout.flush()
    for row_idx in range(X2.shape[0]):
        #row_start_time=time.time()
        if row_idx % inc_size == 0:
            print 'modifying app {}'.format(row_idx)
            print 'current time to modify X dataset so far: {}'.format(time.time()-modx_start_time)
            sys.stdout.flush()
        for fname_idx,fname in fn1_in_fn2:
            if X2[row_idx,fname2_to_index[fname]] != 0:
                X_new[start_row+row_idx,fname_idx]=X2[row_idx,fname2_to_index[fname]]
        #print 'time to modify row: {}'.format(time.time()-row_start_time)
        #sys.stdout.flush()
    print 'time to populate X_new with X2: {}'.format(time.time()-x2_start_time)
    sys.stdout.flush()
    print 'building X_new from X1'
    x1_start_time=time.time()
    for i,j,v in zip(X1.row,X1.col,X1.data):
        if v != 0:
            X_new[i,j]=v
    print 'time to populate X_new with X1: {}'.format(time.time()-x1_start_time)
    print 'time to modify X dataset: {}'.format(time.time()-start_time)
    sys.stdout.flush()

    start_time=time.time()
    h5f_new.create_dataset('X',data=X_new.toarray(),dtype=np.int)
    print 'time to write out new X dataset to file: {}'.format(time.time()-start_time)
    sys.stdout.flush()
    
    dt=h5py.special_dtype(vlen=unicode)
    h5f_new.create_dataset('y',data=y_new,dtype=dt)
    h5f_new.create_dataset('apk_names',data=apk_names_new,dtype=dt)
    h5f_new.create_dataset('fnames',data=fnames1,dtype=dt)
    h5f_new.close()
    h5f1.close()
    h5f2.close()
    print 'total script execution time: {}'.format(time.time()-prog_start_time)
    sys.stdout.flush()

if __name__ == '__main__':
      main()
