#!/usr/bin/env python

import argparse
import utils
import time
import os
import h5py
import numpy as np
import sys

prog_start_time=time.time()
parser = argparse.ArgumentParser(description='converts y labels in an HDF5 file with labels from another file')
parser.add_argument('--hdf',help='HDF5 input file')
parser.add_argument('--label-file','-l',help='new labels file')
parser.add_argument('--fam-labels',help='use family labels, if they are included',dest='fam_labels',action='store_true')
parser.add_argument('--no-fam-labels',help='remove any family labels and use a generic malware label',dest='fam_labels',action='store_false')
parser.add_argument('--create-new-hdf',help='create a new HDF with copied data and replaced labels, which is off by default',action='store_true')
parser.set_defaults(fam_labels=True)
parser.set_defaults(create_new_hdf=False)
args = parser.parse_args()

h5f=h5py.File(args.hdf,'r+')
y=h5f['y'][:]
apk_names=h5f['apk_names'][:]
fnames=h5f['fnames'][:]

apk_names = [n.replace('exploit_apks_','').replace('.apk','').replace('VirusShare_','').strip() for n in apk_names if n.strip()]

#for i,n in enumerate(apk_names):
#    print '{},{}'.format(i,n.strip())

start_time=time.time()
hash_labels=dict()
singleton_count=0
with open(args.label_file,'r') as f:
    for line in f:
        tokens = line.split()
        h = tokens[0]
        label = tokens[1]
        hash_labels[h]=label
        #print '{},{}'.format(h,label)
        if label.startswith('SINGLETON'):
            singleton_count=singleton_count+1
            hash_labels[h]='Malware'
print 'singleton count: {}'.format(singleton_count)
print 'total samples: {}'.format(len(apk_names))
print 'percentage singleton: {:f}'.format( float(singleton_count)/len(apk_names) )
print 'time to load labels: {}'.format(time.time()-start_time)
sys.stdout.flush()

DEBUG = False
if DEBUG:
    print 'label file contents:'
    for h,label in hash_labels.iteritems():
        print '{},{}'.format(h,label)

start_time=time.time()
print 'performing relabeling task...'
for apk_idx,apk_name in enumerate(apk_names):
    if apk_name in hash_labels:
        label = hash_labels[apk_name]
        if DEBUG:
            print 'match: {}, old_label: {}, new_label: {}'.format(apk_name,y[apk_idx],label)
        y[apk_idx]=label
print 'time to perform relabeling: {}'.format(time.time()-start_time)
sys.stdout.flush()

print 'type of y relabeled: {}'.format(type(y))
malware_count = len([l for l in y if l == 'Malware'])
print 'malware count after relabeling: {}'.format(malware_count)
sys.stdout.flush()

start_time=time.time()
dt = h5py.special_dtype(vlen=unicode)
if args.create_new_hdf:
    in_basename = os.path.splitext(args.hdf)[0]
    relabeled_filename=in_basename + '_relabeled.hdf'
    new_f = h5py.File(relabeled_filename,'w')
    h5f.copy('X',new_f)

    new_f.create_dataset('y',data=np.array(y),dtype=dt)
    new_f.create_dataset('apk_names',data=np.array([n.encode('utf8') for n in apk_names]),dtype=dt)
    new_f.create_dataset('fnames',data=np.array(fnames),dtype=dt)
    new_f.close()
else:
    del h5f['y']
    del h5f['apk_names']
    del h5f['fnames']
    
    h5f.create_dataset('y',data=np.array(y),dtype=dt)
    h5f.create_dataset('apk_names',data=np.array([n.encode('utf8') for n in apk_names]),dtype=dt)
    h5f.create_dataset('fnames',data=np.array(fnames),dtype=dt)

h5f.close()
print 'time to store relabeled dataset: {}'.format(time.time()-start_time)
    
sys.stdout.flush()
