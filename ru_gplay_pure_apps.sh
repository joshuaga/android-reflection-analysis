#!/bin/bash
#$ -cwd
#$ -pe openmp 1-4
#$ -q free*,pub*
#$ -j y
#$ -t 1-24999
#$ -r y
#$ -ckpt restart
#$ -l mem_free=32G
SEEDFILE=verified_androzoo_benign_apps.txt
SEED=$(awk "NR==$SGE_TASK_ID" $SEEDFILE)
/share/seal/joshug4/workspace/android-reflection-analysis/ru.sh $SEED
