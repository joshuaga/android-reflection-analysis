#!/usr/bin/env python

import time
import h5py
import argparse
import sys

start_time = time.time()
parser = argparse.ArgumentParser(description='remove a suffix string from every apk name in the apknames dataset of an HDF5 file')
parser.add_argument('--hdf',help='input HDF5 file')
parser.add_argument('--suffix',help='the suffix to be removed')
args = parser.parse_args()

f = h5py.File(args.hdf)
apk_names=f['apk_names']

for i,name in enumerate(apk_names):
    if name.endswith(args.suffix):
        apk_names[i] = name.replace(args.suffix,'',1)
f.close()
print 'time to run: {}'.format(time.time()-start_time)
