#!/bin/bash
#$ -cwd
#$ -q free64,pub64
#$ -j y
#$ -ckpt blcr
# -pe openmp 4

./train_test_split.py $@
