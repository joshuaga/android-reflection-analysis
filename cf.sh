#!/bin/bash
#$ -cwd
#$ -q free64,pub64
#$ -j y
#$ -pe openmp 8-64
#$ -ckpt blcr
# -l mem_size=256
# -l mem_free=200G

#module load enthought_python/7.3.2
./combine_features.py $@
