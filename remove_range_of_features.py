#!/usr/bin/env python

import h5py
import numpy as np
import argparse
import time


prog_start_time=time.time()
parser = argparse.ArgumentParser(description='remove a range of consecutive columns from features of an HDF5 file')
parser.add_argument('--hdf',help='HDF5 file with original data')
parser.add_argument('--new-hdf',help='new HDF5 file with removed features')
parser.add_argument('--start-idx',help='starting index of features to be removed',type=int)
parser.add_argument('--end-idx',help='ending index of features to be removed',type=int)
args = parser.parse_args()
print args

start_time=time.time()
h5f_in = h5py.File(args.hdf,'r')
apk_names = h5f_in['apk_names'][:]
X = h5f_in['X']
y = h5f_in['y']
fnames = h5f_in['fnames']
print 'time to load input HDF5 data: {}'.format(time.time()-start_time)

start_time=time.time()
X_new = np.delete(X,np.s_[args.start_idx:args.end_idx+1],1)
fnames_new = np.delete(fnames,np.s_[args.start_idx:args.end_idx+1])
print 'time to slice out features: {}'.format(time.time()-start_time)

start_time=time.time()
dt = h5py.special_dtype(vlen=unicode)
h5f_new = h5py.File(args.new_hdf,'w')
h5f_in.copy('apk_names',h5f_new)
h5f_in.copy('y',h5f_new)
h5f_new.create_dataset('X',data=X_new,dtype=np.int)
h5f_new.create_dataset('fnames',data=fnames_new,dtype=dt)
h5f_in.close()
h5f_new.close()
print 'time to store new HDF5 data: {}'.format(time.time()-start_time)
print 'total program execution time: {}'.format(time.time()-prog_start_time)



