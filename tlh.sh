#!/bin/bash
#$ -cwd
#$ -q seal,free*,pub*
#$ -j y
#$ -ckpt blcr

module load anaconda/2.7-4.3.1
./test_load_hdf.py $@
