#!/usr/bin/env python

import h5py
import sys
import argparse

parser = argparse.ArgumentParser(description='print apk names with supplied label in an HDF5 file')
parser.add_argument('--hdf',help='input HDF5 file')
parser.add_argument('--label',help='selected label')
args = parser.parse_args()

f = h5py.File(args.hdf,'r')
apk_names = f['apk_names'][:]
y = f['y'][:]

for i,name in enumerate(apk_names):
    if y[i] == args.label:
        print '{} : {}'.format(name,y[i])
