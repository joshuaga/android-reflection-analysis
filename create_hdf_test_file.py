#!/usr/bin/env python

from argparse import ArgumentParser
import h5py
from scipy.sparse import rand
import numpy as np
import time
import utils

parser = ArgumentParser(description='generate HDF5 file for RevealDroid testing purposes')
parser.add_argument('--num-apks',help='number of apks to generate',default=4,type=int)
parser.add_argument('--start-apk-num',help='initial value to use when numbering apks',default=1,type=int)
parser.add_argument('--num-features',help='number of features to generate',default=4,type=int)
parser.add_argument('--start-feat-num',help='initial value to use when numbering features',default=1,type=int)
parser.add_argument('--num-labels',help='number of distinct labels',default=2)
parser.add_argument('--random-seed',help='random seed to be used to generate data',default=None)
args = parser.parse_args()

apks = ['apk{}'.format(i) for i in range(args.start_apk_num,args.start_apk_num+args.num_apks)]
fnames = ['f{}'.format(i) for i in range(args.start_feat_num,args.start_feat_num+args.num_features)]
X_rand = rand(args.num_apks,args.num_features,random_state=args.random_seed,density=0.20)
X_rand = (X_rand*100).ceil().astype(np.int)
y = ['l{}'.format(i) for i in range(1,args.num_labels+1)]

print apks
print fnames
print X_rand.toarray()
print y

timestr = time.strftime("%Y%m%d-%H%M%S")
new_filename='generated_{}.hdf'.format(timestr)
print 'Storing to file {}'.format(new_filename)
utils.store_hdf_data(new_filename,X_rand.toarray(),y,apks,fnames)
