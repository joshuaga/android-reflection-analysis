#!/usr/bin/env python

import h5py
import argparse
import numpy as np
import os
import utils
import time

prog_start_time=time.time()
parser = argparse.ArgumentParser(description='remove benign apps and family-less malware from a dataset and save it to a new file')
parser.add_argument('--hdf',help='input HDF5 file')
args = parser.parse_args()

print args

start_time=time.time()
f = h5py.File(args.hdf,'r')
y = f['y'][:]
apk_names = f['apk_names'][:]
X = f['X']
fnames = f['fnames'][:]
print 'time to load HDF5 data: {}'.format(time.time()-start_time)

start_time=time.time()
mal_indices = []
for i,label in enumerate(y):
    if label != 'Benign' and label != 'benign' and label != 'Malware' and label != 'malware':
        mal_indices.append(i)

y_new = []
apk_names_new = []
for i in mal_indices:
    y_new.append(y[i].encode('utf8'))
    apk_names_new.append(apk_names[i].encode('utf8'))
    
X_new = X[np.array(mal_indices) , : ]
print 'time to build new dataset: {}'.format(time.time()-start_time)

start_time=time.time()
basename = os.path.splitext(os.path.basename(args.hdf))[0]
new_hdf_filename='res/{}_fam-labels-only.hdf'.format(basename)
utils.store_hdf_data(new_hdf_filename,X_new,y_new,apk_names_new,fnames)
print 'time to store new HDF5 data: {}'.format(time.time()-start_time)

print 'total execution time: {}'.format(time.time()-prog_start_time)
