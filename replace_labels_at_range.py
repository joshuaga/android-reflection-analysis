#!/usr/bin/env python

import h5py
import argparse
import time

def main():
    prog_start_time=time.time()
    parser = argparse.ArgumentParser(description='replacel labels of y in an HDF5 file within a range')
    parser.add_argument('--hdf',help='HDF5 file to be modified')
    parser.add_argument('--start-idx',help='index of y to begin modification at',type=int)
    parser.add_argument('--end-idx',help='index of y to end modification at',type=int)
    parser.add_argument('--new-label',help='the new label to use within the range')
    args = parser.parse_args()

    h5f = h5py.File(args.hdf)
    y = h5f['y']

    for i in range(args.start_idx,args.end_idx+1):
        y[i] = args.new_label
    h5f.close()

    print 'total execution time: {}'.format(time.time()-prog_start_time)

if __name__ == '__main__':
      main()
    
