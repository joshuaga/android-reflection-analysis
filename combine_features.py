#!/usr/bin/env python

import argparse
import numpy as np
import sys
import time
import utils
import functools
import os

from os.path import join

'''This script assumes that the datasets do not share any of the same
apps. It will combine the feature space into a unified one, however.

'''

def main():
    prog_start_time=time.time()
    
    parser = argparse.ArgumentParser(description='combine datasets by combining features and instances')
    parser.add_argument('-n1',help='first dataset\'s instance names csv file')
    parser.add_argument('-x1',help='first dataset\'s X csv file')
    parser.add_argument('-y1',help='first dataset\'s y csv file')
    parser.add_argument('-f1',help='first dataset\'s feature names csv file')
    parser.add_argument('-n2',help='second dataset\'s instance names csv file')
    parser.add_argument('-x2',help='second dataset\'s X csv file')
    parser.add_argument('-y2',help='second dataset\'s y csv file')
    parser.add_argument('-f2',help='second dataset\'s feature names csv file')
    parser.add_argument('--new-ds-name',help='new name for the combined data (used to name output files)')
    args = parser.parse_args()

    print 'args: {}'.format(args)

    if not args.new_ds_name:
        raise StandardError('You must supply a dataset name')
    
    print 'loading instance names, y, and feature names, files'
    sys.stdout.flush()
    n1 = np.loadtxt(args.n1,dtype=np.dtype(np.str_),delimiter=',') #[line.rstrip('\n') for line in open(args.n1)]
    n2 = np.loadtxt(args.n2,dtype=np.dtype(np.str_),delimiter=',') #[line.rstrip('\n') for line in open(args.n2)]
    
    y1 = [line.rstrip('\n') for line in open(args.y1)]
    y2 = [line.rstrip('\n') for line in open(args.y2)]
    
    f1 = [line.rstrip('\n') for line in open(args.f1)]
    f2 = [line.rstrip('\n') for line in open(args.f2)]
    
    print 'loading feature data'
    sys.stdout.flush()
    start_time=time.time()
    x1 = np.loadtxt(args.x1,delimiter=',').tolist()
    x2 = np.loadtxt(args.x2,delimiter=',').tolist()
    print 'time to load feature data: {}'.format(time.time()-start_time)
    
    assert len(x1[0]) == len(f1), 'features in x1, and f1, are different lengths'
    assert len(x2[0]) == len(f2), 'features in x2, and f2, are different lengths'
    
    n3=[]
    x3=[]
    y3=[]
    f3=[]

    # SIZE=100
    # n1 = n1[:SIZE]
    # x1 = x1[:SIZE]
    # y1 = y1[:SIZE]

    # n2 = n2[:SIZE]
    # x2 = x2[:SIZE]
    # y2 = y2[:SIZE]
    
    
    f3 = list(set(f1).union(set(f2)))

    print 'combining datasets'
    start_time=time.time()
    n3,x3,y3=combine_par(n1,x1,y1,f1,n3,x3,y3,f3)
    n3,x3,y3=combine_par(n2,x2,y2,f2,n3,x3,y3,f3)    
    print 'time to combine datasets: {}'.format(time.time()-start_time)

    timestr = time.strftime("%Y%m%d-%H%M%S")

    RES='res/'
    np_names_csv = join(RES,os.path.basename(__file__) + '-' + os.path.basename(args.new_ds_name) + '_names_' + timestr + '.csv')
    np_x_csv = join(RES,os.path.basename(__file__) + '-' + os.path.basename(args.new_ds_name) + '_x_' + timestr + '.csv')
    np_y_csv = join(RES,os.path.basename(__file__) + '-' + os.path.basename(args.new_ds_name) + '_y_' + timestr + '.csv')
    np_fnames_csv = join(RES,os.path.basename(__file__) + '-' + os.path.basename(args.new_ds_name) + '_fnames_' + timestr + '.csv')

    print 'saving new dataset'
    start_time=time.time()
    np.savetxt(np_names_csv,n3,delimiter=',',fmt="%s")
    np.savetxt(np_x_csv,x3,delimiter=',',fmt="%f")
    np.savetxt(np_y_csv,y3,delimiter=',',fmt="%s")
    np.savetxt(np_fnames_csv,f3,delimiter=',',fmt="%s")
    print 'time to save new dataset: {}'.format(time.time()-start_time)

    print 'overall running time: {}'.format(time.time()-prog_start_time)

def combine_par(n1,x1,y1,f1,n3,x3,y3,f3):
    def par_comb_data(nxy1,f1,f3):
        apk_name=nxy1[0]
        x_row = nxy1[1]
        label = nxy1[2]
        new_fv = []
        for f in f3:
            if f in f1:
                f1_idx = f1.index(f)
                # use the feature value from f1
                new_fv.append(x_row[f1_idx])
            else:
                # make the feature zero in the new feature vector
                new_fv.append(0)
        return apk_name,new_fv,label
    
    nxy1 = [ (n1[i],x1[i],y1[i]) for i in range(len(x1))]
    new_nxy1 = utils.parmap(functools.partial(par_comb_data,f1=f1,f3=f3),nxy1)
    new_n = [ item[0] for item in new_nxy1]
    new_x = [ item[1] for item in new_nxy1]
    new_y = [ item[2] for item in new_nxy1]
    n3.extend(new_n)
    x3.extend(new_x)
    y3.extend(new_y)

    assert len(n3) == len(x3) == len(y3)
    return n3,x3,y3

def combine_datasets(n3,x3,y3,f3,n1,x1,y1,f1):
    for n1_idx, apk_name in enumerate(n1):
        n3.append(apk_name)
        new_fv = []
        for f in f3:
            if f in f1:
                f1_idx = f1.index(f)
                # use the feature value from f1
                new_fv.append(x1[n1_idx][f1_idx])
            else:
                # make the feature zero in the new feature vector
                new_fv.append(0)
        x3.append(new_fv)
        # use the label for this apk from dataset 1
        y3.append(y1[n1_idx])
    return n3,x3,y3
    
if __name__ == '__main__':
    main()

    
    
                

