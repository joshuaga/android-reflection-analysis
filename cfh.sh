#!/bin/bash
#$ -cwd
#$ -q pub*,free*,seal
#$ -o /share/seal/joshug4/ara_logs/
#$ -e /share/seal/joshug4/ara_logs/
#$ -pe openmp 8-64
#$ -ckpt blcr
# -l mem_size=256
# -l mem_free=200G

#module load enthought_python/7.3.2
./combine_features_hdf.py $@
