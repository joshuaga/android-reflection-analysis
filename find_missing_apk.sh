#!/bin/bash

paths=( $(cat res/pcomp_no_obfs_test.txt | grep -v VirusShare | sed 's#\.apk##g' | grep '\.' | xargs -I{} find /share/seal/joshug4/android/apps/Benign/ -name '{}.apk') )

for p in "${paths[@]}"
do
	echo $p
done
