#!/bin/bash
#$ -cwd
#$ -q seal,free*,pub*
#$ -o /share/seal/joshug4/ara_logs/
#$ -e /share/seal/joshug4/ara_logs/
#$ -ckpt blcr

./time-aware_test.py $@
