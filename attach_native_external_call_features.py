#!/usr/bin/env python

import argparse
import time
import os
import logging
import numpy as np
import utils
import sys
import random

from sklearn.feature_extraction import DictVectorizer

def main():
    start_time = time.time()

    parser = argparse.ArgumentParser(description="attach native external call features to an existing dataset")
    parser.add_argument('-n',help='file with apk names for dataset')
    parser.add_argument('-x',help='file with X dataset')
    parser.add_argument('-y',help='file with y dataset')
    parser.add_argument('-f',help='file with feature names')
    parser.add_argument('-c',metavar='C',default='d',help='select a classifier: k for nearest neighbor, d for decision tree (the default), or s for SVM')
    parser.add_argument('--dir',help='directory containing files with native external call features')
    parser.add_argument('--reduced',help='use reduced set, randomly selected using a random seed of 1',action='store_true',default=False)
    parser.add_argument('--reduced-size',help='set the size, in terms of number of apps, of reduced set',type=int,default=2000)
    args = parser.parse_args()

    timestr = time.strftime("%Y%m%d-%H%M%S")

    logger_file = 'logs/' + os.path.basename(__file__) + '-' + os.path.basename(args.x) + '_' + timestr + '.log'
    logging.basicConfig(filename=logger_file,level=logging.DEBUG,format='%(levelname)s:%(name)s:%(funcName)s:%(message)s')
    print 'this script\'s name: ' + os.path.basename(__file__)
    print 'n file: ' + args.n
    print 'x file: ' + args.x
    print 'y file: ' + args.y
    print 'f file: ' + args.f
    print 'will log to ' + logger_file
    sys.stdout.flush()

    #apk_names = np.loadtxt(args.n,dtype=np.dtype(np.str_),delimiter=',')
    load_prev_dataset_start_time=time.time()
    apk_names = open(args.n).readlines()
    X = np.loadtxt(args.x,delimiter=',')
    y = np.loadtxt(args.y,dtype=np.dtype(np.str_),delimiter=',')
    feature_names=[line.rstrip('\n') for line in open(args.f)]
    print 'time to load previous dataset: {}'.format(time.time()-load_prev_dataset_start_time)

    use_reduced_set = args.reduced
    reduced_size = args.reduced_size
    apk_names_reduced=[]
    X_reduced=[]
    y_reduced=[]
    if use_reduced_set:
        print 'using reduced set...'
        sys.stdout.flush()
        random.seed(1)
        selected_indices=random.sample(xrange(len(apk_names)),reduced_size)
        for i,apk_name in enumerate(apk_names):
            if i in selected_indices:
                apk_names_reduced.append(apk_names[i])
                X_reduced.append(X[i])
                y_reduced.append(y[i])
        apk_names=apk_names_reduced
        X = np.array(X_reduced)
        y = np.array(y_reduced)
                
    #print apk_names
    print X
    print y
    sys.stdout.flush()

    #run_10fold_cv_with_dtree(y, X)

    native_dataset=[]
    native_apk_names=[]
    ds_lim = sys.maxint # dataset size limit
    #ds_lim = 5000 # dataset size limit
    ds_count = 0
    logging.debug("populating native dataset")
    print 'populating native dataset'
    pop_native_ds_start_time=time.time()
    for f in os.listdir(args.dir):
        if f.endswith("_nec.txt"):
            if ds_count >= ds_lim: # stop if dataset size limit is reached
                break
            fd = open(args.dir + f,'r')
            fv=dict()
            for line in fd:
                sline = line.strip()
                if sline: # ensure the line is not empty
                    tokens = sline.split(',')
                    if not len(tokens) == 2:
                        raise AssertionError('line {} has the wrong number of tokens'.format(line))
                    else:
                        key = tokens[0]
                        val = tokens[1]
                        if key == 'apk_name':
                            native_apk_names.append(str(val).replace(".apk",""))
                        else:
                            fv[key]=int(val)
            ds_count += 1
            logging.debug(fv)
            native_dataset.append(fv)
    print 'time to populate native dataset: {}'.format(time.time()-pop_native_ds_start_time)

    if not len(native_apk_names) == len(native_dataset):
        raise AssertionError('native_apk_names length {} is not equal to native_dataset length {}'.format(len(native_apk_names),len(native_dataset)))

    print "running DictVectorizer()"
    sys.stdout.flush()
    vec = DictVectorizer()
    np_nd = vec.fit_transform(native_dataset).toarray()
    #print np_nd

    logging.debug("native feature names:")
    logging.debug(vec.get_feature_names())

    print "combining previous features with native features"
    sys.stdout.flush()
    combine_nonzero_native_start_time=time.time()
    new_apk_names,X_new,y_new=utils.combine_feature_sets(apk_names,native_apk_names,X,np_nd,y)
    new_feature_names=feature_names[:]
    new_feature_names.extend(vec.get_feature_names())
    print 'time to combine nonzero native features with previous features: {}'.format(time.time()-combine_nonzero_native_start_time)

    print 'adding zero features for instances without new features'
    sys.stdout.flush()
    zf_start_time = time.time()
    # new_apk_names_set = set(new_apk_names)
    # zero_features=np.zeros( ( 1,len(np_nd[0]) ) ) # an array of all zeros the length of an np_nd row
    # for i,apk_name in enumerate(apk_names):
    #     if not apk_name in new_apk_names_set:
    #         new_features = np.append(X[i],zero_features)
    #         X_new = np.vstack([X_new,new_features])
    #         y_new = np.append(y_new,y[i])
    #         new_apk_names = np.append(new_apk_names,apk_name)
    new_apk_names,y_new,X_new=utils.add_zero_features_to_missing(np_nd,new_apk_names,apk_names,y_new,X_new,y,X)
    print 'time to add zero features: {}'.format(time.time()-zf_start_time)
    sys.stdout.flush()


    print "storing new features as csv files"
    sys.stdout.flush()
    store_csv_start_time=time.time()

    np_names_csv = os.path.join("res",os.path.basename(args.x) + '-with-native-_names_' + timestr + '.csv')
    np_x_csv = os.path.join("res",os.path.basename(args.x) + '-with-native-_x_' + timestr + '.csv')
    np_y_csv = os.path.join("res",os.path.basename(args.x) + '-with-native-_y_' + timestr + '.csv')
    np_f_csv = os.path.join("res",os.path.basename(args.x) + '-with-native-_fnames_' + timestr + '.csv')

    np.savetxt(np_names_csv,new_apk_names,delimiter=',',fmt="%s")
    np.savetxt(np_x_csv,X_new.astype(np.float_),delimiter=',',fmt="%f")
    np.savetxt(np_y_csv,y_new,delimiter=',',fmt="%s")
    np.savetxt(np_f_csv,np.array(new_feature_names),delimiter=',',fmt="%s")
    print 'csv storage time: {}'.format(time.time()-store_csv_start_time)

    print "running 10fold cv"
    sys.stdout.flush()
    cv_start_time=time.time()
    clf = utils.select_classifier(args.c)
    utils.run_10fold_cv(clf,y_new,X_new,numfolds=args.f)
    print 'cv execution time: {}'.format(time.time()-cv_start_time)

    print('total execution time: %s' % (time.time() - start_time))

if __name__ == '__main__':
    main()
