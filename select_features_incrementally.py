#!/usr/bin/env python

from sklearn.ensemble import ExtraTreesClassifier
from sklearn.datasets import load_iris
from sklearn.feature_selection import SelectFromModel
from sklearn.svm import LinearSVC
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import chi2
from sklearn.feature_selection import VarianceThreshold

from sklearn.linear_model import SGDClassifier
from sklearn.linear_model import PassiveAggressiveClassifier

from scipy.sparse import coo_matrix, vstack

import numpy as np
import h5py
import argparse
import time
import os
import sys
import random

def performFeatureSelection(y, h5f, f, fs_alg):
    feat_sel_start_time=time.time()
    print 'Performing feature selection..'
    sys.stdout.flush()
    lsvc = LinearSVC(C=0.01, penalty="l1", dual=False)
    sel = None
    if fs_alg == 's':
        print 'using SGDClassifier for feature selection'
        sel = SelectFromModel(SGDClassifier(n_jobs=-1))
    elif fs_alg == 'p':
        print 'using passive aggressive classifier'
        sel = SelectFromModel(PassiveAggressiveClassifier())
    sys.stdout.flush()
        
    num_samps=h5f['X'].shape[0]
    step=10000
    idx_pairs = [( low,low+step if (low+step) <= num_samps else num_samps ) for low in xrange(0,num_samps,step)]

    X_new = None
    X_orig_sparse = None
    X_prev_batch = None
    for low,high in idx_pairs:
        print 'Fitting samples {} to {}'.format(low,high-1)
        sys.stdout.flush()
        X_batch=coo_matrix(h5f['X'][low:high])
        y_batch=y[low:high]

        X_orig_sparse=vstack([X_prev_batch,X_batch])
        
        X_new = sel.partial_fit(X_batch,y_batch,classes=np.unique(y))
        X_prev_batch=X_orig_sparse
    X_new = sel.transform(X_orig_sparse.tocsr())

    print 'shape after fs: {}'.format(X_new.shape)
    sup = sel.get_support()
    print 'support: {}'.format(sup)

    selected_feature_names = [n for (i,n) in enumerate(f) if sup[i]]
    #print 'selected feature names:'
    #print selected_feature_names

    print 'number of features selected: {}'.format(len(selected_feature_names))
    assert len(selected_feature_names) == X_new.shape[1]

    print 'time to perform feature selection: {}'.format(time.time()-feat_sel_start_time)

    return X_new,selected_feature_names


parser = argparse.ArgumentParser(description='script for conducting feature selection on hdf5 file')

parser.add_argument('-f',help='HDF5 input file',required=True)
parser.add_argument('--fs-alg',metavar='C',default='s',help='feature selection algorithm: s for SGD classifier; and p for passive agressive classifier')
parser.add_argument('--with-fs',dest='use_fs',action='store_true')
args = parser.parse_args()

start_time=time.time()
print 'loading labels, apk names, and feature names'
h5f = h5py.File(args.f,'r')
X=h5f['X']
y=h5f['y'][:]
apk_names=h5f['apk_names'][:]
fnames=h5f['fnames'][:]
print 'time to load {}: {}'.format(args.f,time.time()-start_time)
sys.stdout.flush()

print 'fnames type: {}'.format(type(fnames))
#print 'fnames: {}'.format([n.decode() for n in fnames])

print 'X.shape: {}'.format(X.shape)
print 'y.shape: {}'.format(y.shape)
#print 'apk_names: {}'.format(apk_names)
sys.stdout.flush()

X_new,sel_features=performFeatureSelection(y,h5f,fnames,args.fs_alg)

sel_features = [x.encode('utf-8') for x in sel_features]

timestr = time.strftime("%Y%m%d-%H%M%S")

start_time=time.time()
in_basename = os.path.splitext(args.f)[0]
reduced_filename=in_basename + '_' + timestr + '_reduced.hdf'
new_f = h5py.File(reduced_filename,'w')
new_f.create_dataset('X',data=X_new.toarray(),dtype=int,compression="gzip", compression_opts=1)
dt = h5py.special_dtype(vlen=unicode)
new_f.create_dataset('y',data=np.array(y),dtype=dt)
new_f.create_dataset('apk_names',data=np.array(apk_names),dtype=dt)
new_f.create_dataset('fnames',data=np.array(sel_features),dtype=dt)
new_f.close()
print 'time to store reduced dataset: {}'.format(time.time()-start_time)
sys.stdout.flush()
