#!/bin/bash
#$ -cwd
#$ -q seal.q
#$ -j y
#$ -pe openmpi 1-32
# -l mem_free=200G
# -ckpt blcr

./attach_native_external_call_features.py $@
