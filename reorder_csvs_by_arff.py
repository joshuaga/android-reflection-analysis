#!/usr/bin/env python

import time
import argparse
import sys
import utils
import os

import numpy as np

def main():
    start_time = time.time()

    parser = argparse.ArgumentParser(description='reorder csv files to have the same order as an input arff file')
    parser.add_argument('--arff',help='input arff file')
    parser.add_argument('-n',help='input csv file with names of dataset')
    parser.add_argument('-x',help='input csv file with X dataset')
    parser.add_argument('-y',help='input csv file with y dataset')
    parser.add_argument('--basename',help='basename for file',default=None)
    args = parser.parse_args()

    print 'loading arff file'
    sys.stdout.flush()
    arff_apk_names,X_arff,y_arff,dataset = utils.extract_arff_data(args.arff)

    print 'loading csv file'
    sys.stdout.flush()
    names = np.loadtxt(args.n,dtype=np.dtype(np.str_),delimiter=',')
    X = np.loadtxt(args.x,delimiter=',')
    y = np.loadtxt(args.y,dtype=np.dtype(np.str_),delimiter=',')

    names = names.tolist()
    X = X.tolist()
    y = y.tolist()

    if len(arff_apk_names) == len(X_arff) == len(y_arff) == len(names) == len(X) == len(y):
        print 'all data is of equal length'
    else:
        print 'some of the data is not of equal length'
        sys.exit(1)

    new_names=[]
    new_X = []
    new_y = []

    print 'reordering data'
    sys.stdout.flush()
    for arff_apk_name in arff_apk_names:
        csv_idx = names.index(arff_apk_name)
        new_names.append(arff_apk_name)
        new_X.append(X[csv_idx])
        new_y.append(y[csv_idx])

    new_names = np.array(new_names)
    new_X = np.array(new_X)
    new_y = np.array(new_y)

    sep = ''
    basename = ''
    if args.basename:
        sep = '_'
        basename = args.basename

    timestr = time.strftime("%Y%m%d-%H%M%S")
    prefix = basename + sep + timestr + '_reordered'


    new_names_csv=os.path.join('res',prefix + '_names.csv')
    new_x_csv=os.path.join('res',prefix + '_x.csv')
    new_y_csv=os.path.join('res',prefix + '_y.csv')

    print 'storing new data'
    np.savetxt(new_names_csv,new_names,delimiter=',',fmt='%s')
    np.savetxt(new_x_csv,new_X.astype(np.float_),delimiter=',',fmt='%f')
    np.savetxt(new_y_csv,new_y,delimiter=',',fmt='%s')

    print 'total execution time: {}'.format(time.time()-start_time)

if __name__ == '__main__':
    main()
