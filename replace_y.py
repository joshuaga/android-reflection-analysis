#!/usr/bin/env python

import time
import argparse
import sys
import numpy as np
import utils
import os
import pandas as pd

def main():
    start_time = time.time()

    parser = argparse.ArgumentParser(description='replace y data from csv files')
    parser.add_argument('-n',help='file with apk names')
    parser.add_argument('-x',help='file with X dataset')
    parser.add_argument('--arff',help='arff file with y dataset to use as replacement')
    args = parser.parse_args()

    arff_apk_names,X_arff,y_arff,ds_arff=utils.extract_arff_data(args.arff)

    print 'loading csv files'
    sys.stdout.flush()
    load_start_time = time.time()
    apk_names = pd.read_csv(args.n,delimiter=',',dtype=np.dtype(np.str_)).values
    X=pd.read_csv(args.x,delimiter=',').values
    print 'time to load csv files: {}'.format(time.time()-load_start_time)
    
    y_new=[]
    print 'replacing dataset'
    sys.stdout.flush()

    limit=sys.maxint
    apk_count=0
    csv_x_indices=dict()
    arff_y_indices=dict()
    anl = list(apk_names.flatten())
    wanl = arff_apk_names.tolist()
    for apk_name in arff_apk_names.tolist():
        if not apk_name in apk_names:
            print '{} is in arff, but not in csv'.format(apk_name)
            continue
        if apk_count >= limit:
            break
        x_idx = anl.index(apk_name)
        y_idx = wanl.index(apk_name)
        csv_x_indices[apk_name]=x_idx
        arff_y_indices[apk_name]=y_idx
        apk_count=apk_count+1

    X_new = np.take(X,csv_x_indices.values(),axis=0)

    for idx in arff_y_indices.values():
        y_new.append( y_arff[idx] )

    arff_basename = os.path.basename(args.arff)
    timestr = time.strftime("%Y%m%d-%H%M%S")    
    new_names_csv = os.path.join("res",os.path.basename(__file__) + '-' + arff_basename + '_names_' + timestr + '.csv')
    new_x_csv = os.path.join("res",os.path.basename(__file__) + '-' + arff_basename + '_x_' + timestr + '.csv')
    new_y_csv = os.path.join("res",os.path.basename(__file__) + '-' + arff_basename + '_y_' + timestr + '.csv')

    new_apk_names=np.array(csv_x_indices.keys()).flatten()
    y_new = np.array(y_new).flatten()

    print 'saving new csv files'
    sys.stdout.flush()
    np.savetxt(new_names_csv,new_apk_names,delimiter=',',fmt='%s')
    np.savetxt(new_x_csv,X_new.astype(np.float_),delimiter=',',fmt='%f')
    np.savetxt(new_y_csv,y_new,delimiter=',',fmt='%s')

    print 'total execution time: {}'.format(time.time()-start_time)

if __name__ == '__main__':
    main()
