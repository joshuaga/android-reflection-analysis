#!/usr/bin/env python

import time
import argparse
import sys
import utils

from sklearn import tree, metrics

def main():
    start_time = time.time()

    parser = argparse.ArgumentParser(description='train and test from an arff file')
    parser.add_argument('--arff',help='input arff file')
    parser.add_argument('--train-size',default=7703,help='select this number of instances as training and the rest as testing')
    args = parser.parse_args()

    print 'loading arff file'
    arff_apk_names,X,y,dataset = utils.extract_arff_data(args.arff)

    sys.stdout.flush()
    X_train = X[:args.train_size]
    y_train = y[:args.train_size]
    X_test = X[args.train_size:]
    y_test = y[args.train_size:]

    print 'running training and testing'
    clf = tree.DecisionTreeClassifier().fit(X_train,y_train)

    predicted = clf.predict(X_test)
    
    print metrics.classification_report(y_test,predicted)

    print 'total execution time: {}'.format(time.time()-start_time)

if __name__ == '__main__':
    main()
