#!/bin/bash
#$ -cwd
#$ -q seal,free*,pub*
#$ -o /share/seal/joshug4/ara_logs/
#$ -e /share/seal/joshug4/ara_logs/
#$ -pe openmp 16-64
# -l mem_size=200G
#$ -ckpt blcr

./attach_features_sparse.py $@
