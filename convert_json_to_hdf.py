#!/usr/bin/env python


import argparse
import time
import sys
import utils


from sklearn.feature_extraction import DictVectorizer
from os.path import splitext
from os.path import basename

import h5py

def main():
    v = DictVectorizer(sparse=False)

    parser = argparse.ArgumentParser(description="convert a directory of log files to a feature vector")
    parser.add_argument('--json',help='json file with dataset',required=True)
    args = parser.parse_args()

    timestr = time.strftime("%Y%m%d-%H%M%S")    

    start_time = time.time()
    arff_apk_names,X,y,feature_names=utils.extract_json_data(args.json)
    print 'time to load json data: {}'.format(time.time()-start_time)
    sys.stdout.flush()
    
    start_time = time.time()
    json_basename=splitext(basename(args.json))[0]
    h5f = h5py.File( 'res/{}_x_{}.hdf'.format(json_basename,timestr),'w' )
    h5f.create_dataset('dataset',data=X)
    h5f.close()
    print 'time to store as HDF5 file: {}'.format(time.time()-start_time)
    sys.stdout.flush()

if __name__ == '__main__':
    main()
          
