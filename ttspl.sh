#!/bin/bash
#$ -cwd
#$ -q free*,pub*
#$ -j y
#$ -ckpt blcr
# -pe openmp 4

./train_test_split.py $@
