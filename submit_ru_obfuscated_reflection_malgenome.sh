#!/bin/bash
#$ -cwd
#$ -pe openmp 4
#$ -q seal,free*,pub*
#$ -j y
#$ -t 1-1109
#$ -r y
#$ -ckpt restart
SEEDFILE=../revealdroid/ref_omg_apps_list.txt
SEED=$(awk "NR==$SGE_TASK_ID" $SEEDFILE)
/share/seal/joshug4/workspace/android-reflection-analysis/ru.sh $SEED
